# coding=utf-8

import datetime as dt
import subprocess
import time

import ccxt
from matrix_client.client import MatrixClient
from tinydb import TinyDB, where

client = MatrixClient("https://anonymous-toiletpaper.me:8448")
token = client.login_with_password(username="PotatoMonster", password="S7kNDBmCdBxLn99FSDLF9uuen5GNrUAdZgGkfVfdPAoCkkCztiPWLYR9ixBbsEMEvobXT3tVUtoZ6c3LxyqC4Q8ZjaFXqbdxPoRH2NmNHeD3erg")
room = client.join_room('!HllKAbZYBWvFftJazW:anonymous-toiletpaper.me')

db = TinyDB('trade_notifier.json')
exchanges = []

finex = ccxt.binance()
finex.secret = ''
finex.apiKey = ''
quote_currencies = ['ETH', 'BTC']
exchanges.append(finex)

while True:
    try:
        symbols = []
        finex.load_markets()
        finex_symbols = finex.symbols

        for i, v in finex.fetch_total_balance().items():
            for sym in finex_symbols:
                if i in sym.split('/')[0] and sym.split('/')[1] in quote_currencies:
                    symbols.append(sym)

        symbols = list(set(symbols))
        all_trades = list()
        for exchange in exchanges:
            for symbol in symbols:
                ret = ([i for i in (exchange.fetch_my_trades(symbol=symbol))])
                if ret:
                    all_trades.extend(ret)

        for i in all_trades:
            if ((dt.datetime.now() - dt.datetime.fromtimestamp(i["timestamp"] / 1000)).total_seconds()) < 120:
                if not db.contains(where('id') == i['id']):
                    db.insert({'id': i['id']})
                    text = f"{i['symbol']} {i['side']} {i['amount']}@{i['price']}"
                    # subprocess.Popen(['notify-send'], text)
                    room.send_text(text)
        print('Waiting for rerun.')
        time.sleep(30)
    except Exception:
        subprocess.Popen(['notify-send', f"I failed. Am i working?"])
        time.sleep(30)
        print('Failed.')
