"""Declare some constants."""

# Twitter accounts to track
twitter_accounts = ['@ethfinex', '@officialmcafee', '@binance', '@BittrexExchange', '@Poloniex',
                    '@BithumbOfficial', '@GDAX', '@Huobi_Pro', '@Bancor', '@OKEx_', '@bitfinex', '@kucoincom', '@LedgerHQ', '@TREZOR',
                    '@Aurora_dao', '@Bitstamp', '@BitZExchange', '@Cryptopia_NZ']

twitter_person_all_communication = ['@aantonop', '@cmcal_bot']
# List of coins to search on binance api
list_of_coins = ['CAG', 'OTN', 'LRX', 'HCC', 'LLT', 'ICO', 'VOISE', 'REP', 'ELC', 'QASH', 'BNC', 'ALIS', 'ETF', 'STAR', 'EDG', 'GRX', 'BTM', 'AVT',
                 'MCAP', 'STX', 'CAT', 'CFD', 'VIBE', 'CHAT', 'DATA', '1ST', 'BCX', 'GNT', 'SBTC', 'SAN', 'GUP', 'PAY', 'CVC', 'KICK', 'FID', 'PIX', 'IFT', 'UKG']

# Return twitter posts from the above account that CONTAIN these words. Have to be lowercase.
# Ex. decentral will match with DECENTRALAND and EdecEntral and decentralizationOFEDEN
# Ex. Decentral, DECENtral, DECENTRAL with match with nothing.
whitelist = ['trading', 'ico', 'coin', 'exchange', 'eth', 'btc', 'crypto', 'currency', 'token', 'white paper', 'deposits',
             'lists', 'invest', 'decentral', 'error', 'downtime', 'server', 'fork', 'airdrop', 'now live', 'launched on huobi', 'launches', 'api',
             'server maintenance', 'listing', 'listed', 'maintenance', 'live on bitfinex', 'start trading', 'trading pairs',
             'tradable against', 'partnership', 'risk', 'security', 'attack', 'compromise', 'hack', 'launch', 'launching', 'launched', 'upgraded', 'upgrade', 'upgrades', 'fake'
                                                                                                                                                                          'problem']

# Same as above but different # dafuq re stef dafuq leei edw xD # MAGIC
blacklist = ['talk', 'discusses', 'friends', 'meet', 'retweet', 'subscribe', 'basic information', 'quizz', 'tickets', 'stream', 'talk', 'host', 'talk', 'win', 'episode']

# Symbols to check for arbitrage opportunities. It will query all their available pairs in all exchanges listed here:
# https://github.com/ccxt/ccxt#supported-cryptocurrency-exchange-markets
symbols = ['BTC', 'KIN', 'LTC', 'ETH', 'SC', 'ETC', 'XMR', 'STRAT', 'KMD', 'BMC']
# Minimum 24h volume in the exchange to be considered.
min_volume_in_usd = 2999
# If arbitrage > percent_difference push to discord. Percent_difference has to be float.
percent_difference = 9

# exchanges that are not to be queried for arbitrage opportunities. It checks if the string given is IN the exchange name.
# Ex. zb matches zbuktu AND uktuzb AND uzbka
blacklist_exch = ['coinsecure', 'vaultoro', 'fyb', 'btcchina', 'zb', '_1broker', 'xbtce', 'bter', 'southxchange', 'coingi',
                  'yunbi', 'flowbtc', 'independentreserve', 'bibox', 'livecoin', 'hitbtc', 'therock', 'okex', 'lykke', 'braziliex', 'bxinth', 'allcoin', 'exx', 'coolcoin']

whitelist_exch = ['poloniex', 'bitfinex', 'idex', 'binance', 'bittrex', 'gdax', 'kraken', 'mercatox', 'bancor', 'okex', 'huobi', 'bitstamp',
                  'bitflyer', 'bit-z', 'liqui', 'wex', 'bibox', 'gdax', 'exmo', 'gate.io', 'cryptopia', 'kucoin', 'bitstamp', 'aex', 'rfinex', 'coss']

# Wallet id's and name to track on omniwallet.
omniwallets = {'Printing': '3MbYQMMmSkC3AgWkj9FMo5LsPTW1zBTwXL'}
omniwallet_ids_names = {'Tether': 31}

# Coinmarketcap coins you want to track.
coin_names = ['ZRX', 'USDT']

# Scheduler time and message you want displayed in utc time
scheduler = {'Asia': 1, 'Europe': 8}
# new_member_message = 'Hello and welcome the the HOTSHOTS.'

# Exchanges you wish to track for new listings. Can choose any of the 100+ supported by ccxt.
listing_exchanges = ['binance', 'bithumb']
