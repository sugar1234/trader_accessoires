# coding=utf-8
"""Sets up all the STUFF."""
import logging.config
import os
import sys

import aiohttp
import async_timeout
import twitter
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from autologging import traced, logged
from discord.ext import commands
from mongoengine import connect
from tinydb import Query, TinyDB

from tracka.configFiles import config
from tracka.configFiles import constants


@traced
@logged
class Starter():
    """Base class that is inherited by everything else."""
    client = commands.Bot(command_prefix='?')
    allowed_ids = config.allowed_ids
    bot_name = config.bot_name
    saved_session = []
    # Cached tickers
    constants = constants
    config = config
    cmc_tickers = {}
    tickers = {}
    scheduler = AsyncIOScheduler(timezone='utc')
    allowed_server_ids = config.allowed_server_ids
    if not os.path.exists(os.getcwd() + '/tracka/dbs/'):
        os.makedirs(os.getcwd() + '/tracka/dbs/')

    if not os.path.exists(os.getcwd() + '/tracka/dbs/pandas/'):
        os.makedirs(os.getcwd() + '/tracka/dbs/pandas/')

    db = TinyDB('tracka/dbs/db.json')
    machine = TinyDB('tracka/dbs/machine.json')
    targetsDB = TinyDB('tracka/dbs/targets.json')
    feedback = TinyDB('tracka/dbs/feedback.json')
    query = Query()

    connect('test')
    # logging.basicConfig(level=logging.INFO, stream=sys.stdout, format="%(levelname)s:%(name)s:%(funcName)s:%(message)s")

    # set up logging to file
    logging.basicConfig(
        filename='log.log',
        level=logging.INFO,
        format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
        datefmt='%H:%M:%S'
    )

    # set up logging to console
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    twitter_accounts = constants.twitter_accounts
    api = twitter.Api(consumer_key=config.twitter_config['consumer_key'],
                      consumer_secret=config.twitter_config['consumer_secret'],
                      access_token_key=config.twitter_config['access_token_key'],
                      access_token_secret=config.twitter_config['access_token_secret'],
                      tweet_mode='extended')

    try:
        if sys.argv[1]:
            testing = False
        logging.warning('Live Mode.')
    except IndexError:
        logging.warning("Testing. Pushing to your spam channel.")
        testing = True

    @staticmethod
    @client.async_event
    async def on_ready():
        """This fires once the discord bot is connected to the channel."""
        print('Logged in as {} with id: {}'.format(Starter.client.user.name, Starter.client.user.id))

    # @staticmethod
    # @client.async_event
    # async def on_command_error(ctx, error):
        # """Error handler for bot commands."""
        # logging.error(error)
        # if '??' in ctx.message.content:
            # return
        # await ctx.message.channel.send(error)

    @staticmethod
    @client.async_event
    async def on_message(message):
        """Handle machine integration."""
        print(message.content)
        await Starter.client.process_commands(message)
        try:
            table = Starter.machine.table(message.channel.name)
            table.insert({'author': message.author.name, 'content': message.content, 'timestamp': str(message.timestamp)})
        except Exception:
            Starter.__log.debug('QuietFail.')

    @staticmethod
    async def fetch(session, url):
        """Fetch the data from the url through some async magic."""
        with async_timeout.timeout(20):
            async with session.get(url) as response:
                return await response.text()

    async def requests_error_handling(self, link):
        """Handle error handling for request library."""
        try:
            async with aiohttp.ClientSession() as client:
                html = await self.fetch(client, link)
            if isinstance(html, str):
                return html
        except Exception:
            self.__log.error(f'{link} failed to connect.')

# @client.event
# async def on_member_join(member):
#    await client.send_message(member, .constants.new_member_message)
