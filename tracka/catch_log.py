"""Handle logging and excepting."""
import logging.config
import os


# Local


def logger():
    """Define the dictionary LOGGGING which is used to set up the logger."""

    LOG_FILENAME = os.getcwd() + 'tracka/dbs/example.log'
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,

        'formatters': {
            'console': {
                'format': '[%(asctime)s][%(levelname)s] %(name)s '
                          '%(filename)s:%(funcName)s:%(lineno)d | %(message)s',
                'datefmt': '%H:%M:%S',
            },
        },

        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'console'
            },
            'file': {
                'level': 'DEBUG',
                # 'class': 'logging.handlers.RotatingFileHandler',
                'class': 'logging.FileHandler',
                'formatter': 'console',
                'filename': LOG_FILENAME,
                # 'maxBytes': 2048,
                # 'backupCount': 3
            },
            'sentry': {
                'level': 'ERROR',
                'class': 'raven.handlers.logging.SentryHandler',
                'dsn': 'https://caa432d2548d4c679c6f0e20e0e0a64e:15f1f937ca534c18a1bf0779e6af47e2@sentry.io/263131',
            },
        },

        'loggers': {
            '': {
                'handlers': ['console', 'sentry', 'file'],
                'level': 'DEBUG',
                'propagate': False,
            },
            'your_app': {
                'level': 'DEBUG',
                'propagate': True,
            },
        }
    }
    return LOGGING


LOGGING = logger()
logging.config.dictConfig(LOGGING)
logger = logging.getLogger(__name__)
