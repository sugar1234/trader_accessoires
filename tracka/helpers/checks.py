# coding=utf-8
"""Some common checks for our bot commands."""

import logging

import discord

from tracka.configFiles import config


def is_boss(ctx):
    """Check if the author of the message has permissions."""
    ret = ctx.message.author.id in config.allowed_ids
    logger_util(ctx, ret, 'am i boss')
    return ret


def can_kick(ctx):
    """Check if the author can use the kick function."""
    ret = ctx.message.author.id in config.can_kick_id
    logger_util(ctx, ret, 'can i kick')
    return ret


def use_pub_command(ctx):
    """Check if the author can use the ?pub add for the public list function."""
    ret = ctx.message.author.id in config.can_add_to_public_target
    logger_util(ctx, ret, 'can i add to public list')
    return ret


def can_read_feedback(ctx):
    """Check if the author can read the feedback of members."""
    ret = ctx.author.id in config.can_read_feedback
    logger_util(ctx, ret, 'can read feedback')
    return ret


def boss_and_private(ctx):
    """Precheck if the user can run the command."""
    # If bot name is the other one in a private conversation
    # If your id is in the allowed ids and the message starts with ?
    ret = ctx.author.id in config.allowed_ids and isinstance(ctx.channel, discord.DMChannel)
    logger_util(ctx, ret, 'if private from boss')
    return ret


async def if_pm(ctx):
    """Check if it's a direct pm to the bot."""
    ret = isinstance(ctx.channel, discord.DMChannel)
    logger_util(ctx, ret, 'if_pm')
    return ret


async def if_no_pm(ctx):
    """Check if it's not a direct pm to the bot."""
    ret = isinstance(ctx.channel, discord.DMChannel)
    logger_util(ctx, ret, 'if_pm')
    return not ret


def logger_util(ctx, ret, name):
    """ Prints a helpful log message."""
    logging.debug(f'{name.capitalize()} returned {ret} for user {ctx.author}')
