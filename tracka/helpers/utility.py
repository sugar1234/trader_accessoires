"""Help the main."""
from datetime import datetime

import mongoengine as me
import pytz
from autologging import logged, traced

from tracka.helpers.exc_dec import exception
from tracka.item_classes.functions import Functions
from tracka.item_classes.server_preferences import ServerPrefs
# Local Imports
from tracka.setup import Starter


@logged
@traced
class BaseFunction(Starter):
    """Define some common functions for all Function items."""

    @exception()
    def interesting(self, text):
        """Define keywords to look for in twitter tweets."""
        for i in self.constants.whitelist:
            if i in text.lower():
                for j in self.constants.blacklist:
                    if j not in text.lower():
                        return 1
        return 0

    @exception()
    def calculate_global_times(self):
        """Calculate global times and formats then adequately."""
        utc_dt = pytz.utc.localize(datetime.utcnow())
        date = utc_dt.astimezone(pytz.timezone('Europe/Berlin')).strftime('%d/%m')
        berlin_time = utc_dt.astimezone(pytz.timezone('Europe/Berlin')).strftime('%H:%M')
        moscow_time = utc_dt.astimezone(pytz.timezone('Europe/Moscow')).strftime('%H:%M')
        new_york_time = utc_dt.astimezone(pytz.timezone('America/New_York')).strftime('%H:%M')
        tokio_time = utc_dt.astimezone(pytz.timezone('Japan')).strftime('%H:%M')
        timestamps = 'Date: {}, Berlin: {}, Moscow: {}, NY: {}, Tokio: {}'.format(date, berlin_time, moscow_time, new_york_time, tokio_time)
        return timestamps

    @exception()
    async def push(self, obj, func_name, payload=None, extra=''):
        """New and improved save_to_db."""
        # self.__log.error(func_name, extra)
        for item in ServerPrefs.objects(function_name=func_name, extra=extra):
            channel = self.client.get_channel(item.channel_id)
            try:
                Functions(server_id=channel.guild.id, data_id=obj['id'], function_name=func_name).save()
            except (me.NotUniqueError, AttributeError):
                break

            # if self.testing:
                # channel = self.client.get_channel(self.config.discord_channel_ids['spam'])
            if payload:
                payload.set_footer(text=self.calculate_global_times())
                await channel.send(obj['text'], embed=payload)
            else:
                await channel.send(obj['text'])

    @staticmethod
    async def percent_change(a, b):
        """Returns a simple percent change."""
        return ((a - b) / b) * 100
