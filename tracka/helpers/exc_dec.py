"""Handle exception decorator."""
import inspect
import traceback
from autologging import logged, traced
from ccxt import BaseError


# @logged
# @traced
def exception():
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur

    @param logger: The logging object
    """

    def decorator(func):
        """Decorate the function with the below functions."""

        if inspect.iscoroutinefunction(func):
            async def wrapper(*args, **kwargs):
                """Wrap the function in try except."""
                try:
                    return await func(*args, **kwargs)
                except BaseError:
                    # logger.debug('ccxt had an error. Nothing to worry about.')
                    return -1
                except Exception as e:
                    # log the exception
                    print(traceback.print_exc())
                    # logger.debug(func.__name__)
                    # logger.debug(traceback.format_exc())
                    return -1
        else:
            def wrapper(*args, **kwargs):
                """Wrap the function in try except."""
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    print(traceback.print_exc())
                    # log the exception
                    # logger.debug(func.__name__)
                    # logger.debug(traceback.format_exc())
                    return -1
        return wrapper
    return decorator


def main_function_exception():
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur

    @param logger: The logging object
    """

    def decorator(func):
        """Decorate the function with the below functions."""

        if inspect.iscoroutinefunction(func):
            async def wrapper(*args, **kwargs):
                """Wrap the function in try except."""
                try:
                    return await func(*args, **kwargs)
                except Exception:
                    # log the exception
                    # logger.info(func.__name__ + ' failed. If you spot a trend here please report it.')
                    # logger.debug(func.__name__ + traceback.format_exc())
                    return -1
        else:
            def wrapper(*args, **kwargs):
                """Wrap the function in try except."""
                try:
                    return func(*args, **kwargs)
                except Exception:
                    # log the exception
                    # logger.info(func.__name__ + ' failed. If you spot a trend here please report it.')
                    # logger.debug(func.__name__ + traceback.format_exc())
                    return -1
        return wrapper
    return decorator
