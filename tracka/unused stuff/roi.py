"""Calculate roi."""
import logging
import pickle

import ccxt.async as ccxt
import humanfriendly
import requests
from bs4 import BeautifulSoup
from discord.ext import commands
from setup import client

logger = logging.getLogger(__name__)
filename = 'tracka/dbs/{}.db'.format('roi')
allowed_ids = configFiles.config.allowed_ids


@client.group(pass_context=True)
@commands.check(bot_commands.utility.check_if_it_is_a_boss)
async def roi(ctx):
    """Calculate roi, add coins to database, list coins from database."""
    if ctx.invoked_subcommand is None:
        await client.say('Invalid roi command passed.')


@roi.command()
async def ls():
    """Handle list command."""
    try:
        with open(filename, 'rb') as f:
            data = pickle.load(f)
            text = ''
            for i, v in data.items():
                text += '{} : {:.7f}$\n'.format(i, v)
            print(text)
            await client.say(text)

    except FileNotFoundError:
        await client.say('Nothing found.')


@roi.command()
async def add(name: str, price: float):
    """Add coin and price to the database."""
    data = dict()
    try:
        with open(filename, 'rb') as f:
            data = pickle.load(f)
    except FileNotFoundError:
        data = dict()
    with open(filename, 'wb') as f:
        data.update({name: price})
        pickle.dump(data, f)
    await client.say("Added {} with starting price of {}!".format(name, price))


@roi.command()
async def deleteAllWithoutConfirm():
    """DELETES ALL."""
    with open(filename, 'wb') as f:
        pickle.dump({}, f)


@roi.command()
async def show(coin: str):
    """Show info about coin."""
    base_symbol = coin.upper()
    base_usd = await(ccxt.coinmarketcap().fetch_ticker(base_symbol + '/USD'))
    base_usd = float(base_usd['info']['price_usd'])
    try:
        with open(filename, 'rb') as f:
            data = pickle.load(f)
    except FileNotFoundError:
        logger.info('Please create a database first with roi add BMC 1.')
    print(data)
    percent_difference = ((base_usd - data[base_symbol]) / data[base_symbol]) * 100
    percent_difference = humanfriendly.format_number(percent_difference)
    page = requests.get('https://www.cryptomiso.com')
    soup = BeautifulSoup(page.text, 'lxml')
    for a in soup.find_all('h4'):
        name = (a.find('a').text)
        if name == base_symbol:
            rank = (a.text.split('\n')[1].strip().split('.')[0])
            commits = a.find('span').text.split(' ')[0]
    await client.say('**{}**\n{:.7f} vs. {:.7f} for a total of {}% roi.\nRank: {}, Commits: {}'.format(base_symbol, base_usd, data[base_symbol], percent_difference, rank, commits))



@roi.command()
async def help():
    """Show command usage to the noobs."""
    await bot_commands.utility.help(client, 'roi', 'return on investment', ['coin', 'price'])
