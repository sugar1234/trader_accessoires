# coding=utf-8
"""Track btc mempool, fees."""
import json
import random

import discord
from autologging import traced, logged

# Local Imports
from tracka.helpers.exc_dec import exception
from tracka.helpers.utility import BaseFunction


@traced
@logged
class BTCStatsFunction(BaseFunction):
    """Provide actual fees, mempool stats."""

    @exception()
    def __init__(self):
        """Initiate all scheduled jobs for btc stats."""
        # self.scheduler.add_job(self.add_to_async_loop, args=['1m'])  # , trigger='cron', hour='*/6')
        self.scheduler.add_job(self.add_to_async_loop, args=['6 hours'], trigger='cron', hour='*/6', minute='1')
        self.scheduler.add_job(self.add_to_async_loop, args=['12 hours'], trigger='cron', hour='*/12', minute='2')
        self.scheduler.add_job(self.add_to_async_loop, args=['24 hours'], trigger='cron', hour='0', minute='3')
        self.scheduler.add_job(self.add_to_async_loop, args=['1 week'], trigger='cron', day_of_week='0', hour='0', minute='4')
        self.table = self.db.table('btc_stats')

    @exception()
    async def caller(self):
        """Call chain, bitcoin fees up for info."""
        unconfirmed = await self.requests_error_handling('https://chain.api.btc.com/v3/tx/unconfirmed/summary')
        fees = await self.requests_error_handling('https://bitcoinfees.earn.com/api/v1/fees/recommended')
        fees = json.loads(fees)
        unconfirmed = json.loads(unconfirmed)['data']
        return fees, unconfirmed

    def add_to_async_loop(self, interval):
        """Is needed for apscheduler."""
        self.client.loop.create_task(self.main(interval))

    async def main(self, interval, dont_save=''):
        """Gets called by main."""
        fees, unconfirmed = await self.caller()
        fastest = fees['fastestFee']
        half_hour = fees['halfHourFee']
        hour = fees['hourFee']
        tx_size = unconfirmed['size'] / 1000000
        tx_count = unconfirmed['count']

        payload = discord.Embed(colour=random.randint(0, 16777215))
        description = 'FastestFee: **{} sat**\nHalfHourFee: **{} sat**\nHourFee: **{} sat**\n'.format(fastest, half_hour, hour)
        title = 'BTC fee statistics.'
        payload.add_field(name=title, value=description)
        description = 'Uncofirmed TXs: **{:,}**\nSize: **{:.2f} MB**\n'.format(tx_count, tx_size)
        title = 'BTC Mempool statistics.'
        payload.add_field(name=title, value=description)

        data = {'id': interval, 'comparables': {'tx_size': tx_size, 'tx_count': tx_count}, 'text': 'Stats for interval of: {}'.format(interval)}
        if dont_save:
            await self.client.say(embed=payload)
        else:
            channel = discord.Object(id=self.config.discord_channel_ids['coinmarketcap'])
            await self.change(self.table, channel, data, payload)
        self.__log.info('BTC fee, mempool finished correctly.')

    @exception()
    async def change(self, table, channel, data, payload, text=''):
        """Print current data and calculate percent difference of data."""
        data_old = table.get(self.query.id == data['id'])
        self.__log.debug('{}, {}'.format(data, data_old))
        if data_old:
            for key, value in data['comparables'].items():
                text += '{}: **{:.2f}%**\n'.format(key, await self.percent_change(value, data_old['comparables'][key]))
            payload.add_field(name='Percent Differences:', value=text)
        table.upsert(data, self.query.id == data['id'])
        await self.client.send_message(channel, data['text'], embed=payload)
