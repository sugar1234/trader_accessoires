"""Misc functions."""
import tracka.functions.btc_stats as btc_stats
from discord.ext import commands

from tracka.bot_commands.utility import BaseCommand


class MiscCommand():
    """Handle misc commands that don't belong to other categories."""
    # %TODO make it pass client to the function.

    checker = BaseCommand()

    def __init__(self, client):
        self.client = client

    @commands.group(pass_context=True)
    @commands.check(checker.check_if_it_is_a_boss)
    async def m(self, ctx):
        """Perform helpful tasks that don't belong in another category."""
        if ctx.invoked_subcommand is None:
            await self.client.say('Invalid misc command passed.')

    @m.command()
    async def bstat(self):
        """
        Show btc stats. Current fees and mempool.

        Example Usage:
        ?b bstat
        """
        statObj = btc_stats.BTCStatsFunction()
        await statObj.main('', True)


def setup(client):
    client.add_cog(MiscCommand(client))
