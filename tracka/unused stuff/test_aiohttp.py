import asyncio

import aiohttp
import async_timeout


class nutMeg():
    async def fetch(self, url):
        """Fetch the data from the url through some async magic."""
        with async_timeout.timeout(20):
            async with self.session.get(url) as response:
                return await response.text()

    async def requests_error_handling(self, link):
        """Handle error handling for request library."""
        # try:
        try:
            print(self.session)
        except AttributeError:
            self.session = aiohttp.ClientSession()
        print(type(self.session))
        html = await self.fetch(link)
        if isinstance(html, str):
            return html
        # except Exception as e:
        #    print(e)

    async def yo(self):
        a = await self.requests_error_handling('https://google.com')
        print(a)

    async def kill(self):
        await self.session.close()


obj = nutMeg()
loop = asyncio.new_event_loop()
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.yo())
loop.run_until_complete(obj.kill())
