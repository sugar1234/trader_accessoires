"""Handle trends alerting to discord."""
from datetime import datetime

import mongoengine as me
import pytrends.request as py
from autologging import traced, logged

from tracka.item_classes.trend import Trend, Search


# Local Imports


@traced
@logged
class TrendAlertsFunction():
    """Handle trends alerting to discord."""

    def __init__(self):
        self.pytrend = py.TrendReq()
        self.time_frames = ['now 1-d', 'now 7-d', 'today 1-m']  # , 'now 1-H', 'now 4-H'
        self.search_terms = ['ripple']  # , 'blackmoon']
        pass

    @staticmethod
    def calc(a, b):
        """Percent difference."""
        if a == 0:
            return b
        return (b - a) / a

    @staticmethod
    def average(a):
        """Average."""
        return sum(a) / len(a)

    def main(self):
        """Start the sequence."""
        me.connect('aaac')
        for search_term in self.search_terms:
            for time_frame in self.time_frames:
                self.runner(search_term, time_frame)
        self.__log.info('Trend alert database creation done.')

    def runner(self, search_term, time_frame):
        """Run for each timeframe/search_term pair."""
        author_name = 'koloxarto1'
        author_id = '432423423423'
        search = Search.objects(time_frame=time_frame, search_term=search_term)
        if search.count() == 0:
            self.pytrend.build_payload(kw_list=[search_term], timeframe=time_frame)
            df = self.pytrend.interest_over_time()
            values = df[search_term].tolist()
            dates = df.index.tolist()
            percent = self.calc(self.average(values[:5]), self.average(values[-5:]))
            search = Search(search_term=search_term, time_frame=time_frame, last_updated=datetime.now(), values=values, date_values=dates, percent_change=percent)
            search.save()
        else:
            search = search.get()

        updated = Trend.objects(author_name=author_name, author_id=author_id).update_one(add_to_set__search_terms__=search)
        if not updated:
            trend = Trend(search_terms=[search], author_name=author_name, author_id=author_id)
            trend.save()


cc = TrendAlertsFunction()
cc.main()
