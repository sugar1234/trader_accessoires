"""Offer database services."""
import datetime
import os
import random
import re
from operator import itemgetter

import discord
import texttable as tt
from discord.ext import commands
from tinydb import TinyDB

from tracka.bot_commands.utility import BaseCommand


class MakerCommands(BaseCommand):
    """Handle ?s command."""

    def __init__(self, client):
        """Marker Commands
        Add: `?s add category/subcategory/coin` followed by a wall of text with links.
        ListAll: `?s ls`
        List for user: `?s ls user`
        Show only 1 post: `?s show ID user`
        Show everything matching search_term: `?s show search_term`
        Show all Charts for search_term: `?s show search_term charts`
        Show all Charts for search_term and user: `?s show search_term user charts`
        Show one chart for user and id: `?s show ID user charts`
        Variables
        category: bitfinex, bittrex, etc.
        subcategory: margin, leverage, etc.
        coin: btc, eth, xrp, etc.
        ID: The id of the post as shown by `?s ls [username]`
        search_term: btc, bitfinex, btc/bitfinex, etc. Can be any combination of (sub-)category and coin.
        user: The username as shown in the user's profile in discord.
        Examples
        A: `?s add btc/bitfinex info`
        B: `?s add bittrex/btc`
        C: `?s show btc`
        D: `?s show 1 koloxarto charts`
        E: `?s show 2 koloxarto`
        F: `?s show bitfinex`
        Tips
        ShowTip: `?s show btc` matches everything with btc in its name. so btc and btc/bitfinex
        AddTip: category/subcategory/coin can be used in any order.
        GeneralTip: You should not use spaces in any command argument. btc/bitfinex is ok. btc/bit finex is not.
        """

        self.client = client
        # self.db = TinyDB('tracka/dbs/maker.json')
        # self.query = Query()

    @commands.group(pass_context=True)
    async def s(self, ctx):
        """Create and manipulate a database of charts. THIS IS EXPERIMENTAL."""
        if ctx.invoked_subcommand is None:
            await self.client.say('Invalid marker command passed.')

    # @s.command(pass_context=True)
    # # %TODO FIX THIS
    # async def remove(ctx, coin: str):
    # """Delete a coin from the list."""
    # self.table.update(delete(coin.upper()), self.query.id == ctx.message.author.id)

    # @s.command(pass_context=True)
    # async def deleteAll(ctx):
    # """Delete Everything. This is for testing and will be removed"""
    # table = self.db.table(ctx.message.author.id)
    # table.purge()

    @s.command(pass_context=True)
    async def add(self, ctx, category: str, coin: str, pid: str, *extra: str):
        """
    Add an entry to the database.

    Keyword Arguments:
    category -- directory with multiple subdirectories
    coin     -- coin name
    pid      -- unique keyword you can use to find the post again
    extra    -- the wall of text you want to add

    Examples:
    ?s add bitfinex btc id1929 wall of text
    ?s add bittrex/margin eth id1028 wall of text with links
    """
        filename = os.getcwd() + 'tracka/dbs/maker/' + category + '/'
        # %TODO If permissioned add category
        if not os.path.exists(filename):
            os.makedirs(filename)
        db = TinyDB(filename + coin + '.json')
        extra = " ".join(extra)
        db.insert({'a_name': ctx.message.author.name, 'coin': coin, 'uid': ctx.message.author.id,
                   'pid': pid, 'category': category, 'info': extra, 'time': datetime.datetime.now()})
        db = TinyDB('tracka/dbs/maker/index.json')
        db.insert({'pid': pid, 'location': filename + coin + '.json', 'coin': coin, 'a_name': ctx.message.author.name})
        await self.client.say('Added {} for category {}.'.format(coin, category))

    @s.command(pass_context=True)
    async def ls(self, ctx, typ: str, term: str):
        """
    Handle list command.

    Keyword Arguments:
    term -- username or coin you want to search for
    typ  -- use 'name' if you want to search for username. 'coin' for coin.
    """

        db = TinyDB('tracka/dbs/maker/index.json')
        saved = []
        if typ == 'name':
            items = db.search(self.query.a_name == term)
            f = []
            for item in items:
                f.append(item['location'])
            for i in list(set(f)):
                db = TinyDB(i)
                saved.extend(db.search(self.query.a_name == term))
        elif typ == 'coin':
            items = db.search(self.query.coin == term)
            f = []
            for item in items:
                f.append(item['location'])
            for i in list(set(f)):
                db = TinyDB(i)
                saved.extend(db.search(self.query.coin == term))

        newitems = sorted(saved, key=itemgetter('time'))
        tab = tt.Texttable()
        tab.add_row(['Categoy', 'Name', 'Coin', 'ID'])
        for item in newitems:
            tab.add_row([item['category'], item['a_name'], item['coin'], item['pid']])
            tab.set_deco(tt.Texttable.VLINES)
        s = '```' + tab.draw() + '```' + '\n'
        payload = discord.Embed(title='All categories for {} sorted by time.'.format(term), description=s, colour=random.randint(0, 16777215))
        await self.client.say(embed=payload)

    @s.command(pass_context=True)
    async def show(self, ctx, pid: str):
        """
    Shows the entry matching.

    Keyword Arguments:
    pid      -- unique keyword you can use to find the post again

    """
        db = TinyDB('tracka/dbs/maker/index.json')
        item = db.get(self.query.pid == pid)

        db = TinyDB(item['location'])
        item = db.get(self.query.pid == item['pid'])
        payload = discord.Embed(title='Matching post.', colour=random.randint(0, 16777215))

        tab = tt.Texttable()
        tab.add_row(['Info'])
        tab.add_row([item['a_name']])
        tab.add_row([item['category']])
        tab.add_row([item['pid']])
        tab.add_row([datetime.datetime.fromtimestamp(item['time']).strftime('%d/%m %H:%M')])
        s = '```' + tab.draw() + '```'

        await self.client.say(s)
        await self.client.say('```Press 1 for the whole text\nPress 2 for just the links.```')
        msg = await self.client.wait_for_message(author=ctx.message.author)
        if msg.content == '1':
            await self.client.say(item['info'])
        elif msg.content == '2':
            url = item['info']
            urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', url)
            for url in urls:
                await self.client.say(url)


def setup(client):
    client.add_cog(MakerCommands(client))
