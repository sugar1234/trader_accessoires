"""Coinmarketcal crawler."""
import asyncio
import random

import dateutil.parser as dparser
import discord
from autologging import traced, logged
from bs4 import BeautifulSoup

# Local Imports
from tracka.helpers.exc_dec import exception
from tracka.helpers.utility import BaseFunction


@traced
@logged
class CMCalFunction(BaseFunction):
    """Coinmarketcal crawler."""

    def __init__(self):
        self.table = self.db.table('twitter_post')

    @exception()
    async def coinmarketcal_inner(self, _div):
        """Track coinmarketcal for news."""
        image_link = ''
        desc = ''
        useful = {'titles': [], 'links': []}
        for i in _div.find_all("h5"):
            useful['titles'].append(i.get_text().strip())
        for i in _div.find_all("p", class_="description"):
            desc = i.get_text().strip()
        for i in _div.find_all("a"):
            if 'images' in i['href']:
                image_link = i['href']
            if 'http' in i['href']:
                useful['links'].append(i['href'])
        image_link = "http://coinmarketcal.com" + image_link
        time_in_the_future = dparser.parse(useful['titles'][0], fuzzy=True, dayfirst=True)
        if useful['titles'] and useful['links']:
            payload = discord.Embed(title=useful['titles'][2], description=desc, url=useful['links'][0],
                                    timestamp=time_in_the_future, colour=random.randint(0, 16777215))
            payload.set_image(url=image_link)
            await self.push({'id': useful['titles'][1:], 'text': useful['titles'][1]}, self.table, discord.Object(id=self.config.discord_channel_ids['coincal']), payload)

    @exception()
    async def caller(self):
        """Call coinmarketcal up fro info."""
        reply = await self.requests_error_handling('http://coinmarketcal.com/?form%5Bmonth%5D=&form%5Byear%5D=&form%5Bsort_by%5D=created_desc&form%5Bsubmit%5D=')
        soup = BeautifulSoup(reply, "lxml")
        mydivs = soup.findAll("article")
        return mydivs

    @exception()
    async def main(self):
        tasks = []
        for cnt, _div in enumerate(await self.caller()):
            tasks.append(self.coinmarketcal_inner(_div))
            if cnt >= 3:
                break
        await asyncio.gather(*tasks)
        self.__log.info('CoinmarketCal din dan.')
