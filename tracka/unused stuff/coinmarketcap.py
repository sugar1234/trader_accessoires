"""Handle CMC."""
import asyncio

import discord
from autologging import traced, logged
# Local Imports
from helpers.exc_dec import exception
from helpers.utility import BaseFunction


@traced
@logged
class CoinmarketcapAlerts(BaseFunction):
    """Handle coinmarketcap alerts. Could become a beast if left unchecked."""
    @exception()
    async def main(self):
        await asyncio.sleep(10)
        for key, value in self.cmc_tickers.items():
            if float(value['info']['rank']) < 200:
                change = (float(value['info']['percent_change_1h']))
                if change > 30.0:
                    payload = discord.Embed()
                    await self.push({'id': self.below, 'text': ''}, self.db.table('cmc_change'), self.channel, payload)
        self.__log.info('CMC DONE.')
