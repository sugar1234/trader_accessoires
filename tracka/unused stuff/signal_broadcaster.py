"""Handle signal reporting to discord."""
import logging
import pickle

import configFiles.config as config
import discord
# Local Imports
from helpers.exc_dec import main_function_exception
from helpers.utility import save_to_db

logger = logging.getLogger(__name__)


@main_function_exception(logger)
async def signals(client, testing):
    """Open the file with the newest signals saved and propagate them to discord."""
    with open('tracka/dbs/saved_signals.db', 'rb') as f:
        text = (pickle.load(f))
        send = ''
        for timestamp, coins in text.items():
            send += timestamp.split('+')[0] + ':\n'
            for name, values in coins.items():
                send += ('\t**{}:**\n'.format(name.upper()))
                for key, value in values.items():
                    send += ('\t\t{}:{}\n'.format(key, value))
        await save_to_db(text_to_check=send, text_to_write=send, filename='tracka/dbs/signals.db', disc_channel=discord.Object(id=config.discord_channel_ids['spam']), client=client, testing=testing)
