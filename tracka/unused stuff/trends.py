"""Handle google trends."""
import datetime
import pickle
import random

import ccxt
import discord
import pandas as pd
import plotly
import plotly.graph_objs as go
from autologging import logged, traced
from discord.ext import commands
from plotly import tools
from tinydb import TinyDB

from tracka.bot_commands.utility import BaseCommand


@traced
@logged
class TrendsCommand():
    filename = 'tracka/dbs/{}.db'.format('trends')

    def __init__(self, client):
        """Trends Commands
        List Terms: ?g ls
        Add Term: ?g add search_term
        Delete Term: ?g remove search_term
        Show Graph: ?g show search_term
        Compare Terms: ?g comp search_termA search_termB
        Delete Everything: ?g deleteAll
        Plot Pair/Search_Term: ?g plot pair search_term shift_days
        Variables
        search_term: btc, 'how to buy btc', 'how to recover hacked wallets', etc.
        pair: a pair from binance. BTC/USDT, LTC/USDT, etc.
        shift_days: How many days you want to shift the popularity graph ahead.
        Examples
        A: ?g add 'how to buy btc'
        B: ?g show 'how to buy btc'
        C: ?g comp btc ltc
        D: ?g plot BTC/USDT btc 2
        Tips
        SearchTerm: If the search_term is more than one word wrap it in enclosing ''. Eg. 'how to buy btc'
        """
        self.client = client

        plotly.tools.set_credentials_file(username='swilso793', api_key='OjKiOPJdUisoF7AUO443')
        self.db = TinyDB('tracka/dbs/trends.json')

    check = BaseCommand()

    async def g(self, ctx):
        """Google trends shenanigans."""
        if ctx.invoked_subcommand is None:
            await self.client.say('Invalid trends command passed. Try using: `?g help`')

    async def add(self, ctx, elem: str):
        """Add a search term."""
        table = self.db.table(ctx.message.author.id)
        table.insert({'term': elem, 'name': ctx.message.author.name, 'type': 'public'})
        await self.client.say("Added {}!".format(elem))

    async def remove(self, ctx, elem: str):
        """Delete a search term."""
        await self.client.say('Remove not working for now.')
        return
        # %TODO FIX THIS SAME AS IN TARGETS
        # self.table.update(delete(elem), self.query.id == ctx.message.author.id)
        # await self.client.say('{} deleted from list.'.format(elem))

    async def ls(self, ctx):
        """Handle list command."""
        table = self.db.table(ctx.message.author.id)
        payload = discord.Embed(title='List of Search Terms.', colour=random.randint(0, 16777215))
        for row in table:
            payload.add_field(name=row['term'], value=row.doc_id)  # await self.return_percent(v))
        await self.client.say(embed=payload)

    async def show(self, ctx, search: str):
        """Handle show command."""
        time_frames = ['now 1-d', 'now 7-d', 'today 1-m']  # , 'now 1-H', 'now 4-H'
        fig = tools.make_subplots(rows=len(time_frames), cols=1, subplot_titles=(time_frames), shared_yaxes=True)

        # show a plot with a subplot for each time_frame
        for cnt, j in enumerate(time_frames):
            filename = 'tracka/dbs/pandas/{}_{}.{}'.format(search, j, 'pkl')
            with open(filename, 'rb') as f:
                newDF = (pickle.load(f))
            trace = (go.Scatter(x=newDF[search].index, y=newDF[search], name=search))  # legendgroup=search, marker={'color': colours[i]}))
            fig.append_trace(trace, cnt + 1, 1)

        fig['layout'].update(title=search)
        await self.plot_and_send(search, fig, ctx.message.channel)

    async def comp(self, ctx, *search_terms):
        """Like show but with multiple stuff."""
        time_frames = ['now 1-d', 'now 7-d', 'today 1-m']  # , 'now 1-H', 'now 4-H'
        fig = tools.make_subplots(rows=len(time_frames), cols=1, subplot_titles=(time_frames), shared_yaxes=True)
        colours = ['black', 'red', 'blue', 'green', 'yellow', 'brown']

        # %TODO change ugly hacks to a better solution
        for cnt, j in enumerate(time_frames):
            tasks = []
            for search in search_terms:
                filename = 'tracka/dbs/pandas/{}_{}.{}'.format(search, j, 'pkl')
                with open(filename, 'rb') as f:
                    tasks.append(pickle.load(f))
            newDF = pd.concat(tasks, axis=1)
            trace = []
            i = 0
            for search in search_terms:
                trace.append(go.Scatter(x=newDF[search].index, y=newDF[search], name=search, legendgroup=search, marker={'color': colours[i]}))
                i += 1
            for i in trace:
                fig.append_trace(i, cnt + 1, 1)

        fig['layout'].update(title=" ".join(search_terms))
        await self.plot_and_send(search, fig, ctx.message.channel)

    async def deleteAll(self, ctx):
        """Delete the corresponding user table."""
        await self.deleteAll(ctx.message.author.id)

    async def plot(self, ctx, coin: str, search: str, shift_days: int):
        """Plots corelation between price and popularity."""
        # Get and graph the coin price from binance
        timestamp, closep = await self.retrieve_ccxt_data(coin)
        price = go.Scatter(x=timestamp, y=closep, yaxis='y2', name='price')

        # Get and graph the google trends data for search
        filename = 'tracka/dbs/pandas/{}_{}.{}'.format(search, 'now 7-d', 'pkl')
        with open(filename, 'rb') as f:
            newDF = (pickle.load(f))
        newDF = newDF.resample('24H').mean()
        newDF.index = newDF.index.shift(shift_days, freq=datetime.timedelta(days=1))
        popularity = go.Scatter(x=newDF[search].index, y=newDF[search], name='popularity')

        # Make logarithmic graph with 2 yaxis
        layout = go.Layout(title='Popularity vs Price for {}'.format(coin), yaxis=dict(type='log', autorange=True, title='Google Trends for {}'.format(search)),
                           yaxis2=dict(type='log', autorange=True, title='Price for {}'.format(coin), overlaying='y', side='right'))
        fig = go.Figure(data=[price, popularity], layout=layout)

        await self.plot_and_send(search, fig, ctx.message.channel)

    # %TODO find a way to differentiate if the search term given is a number or a string
    # async def search_from_id(self, table, search):
    # if
    # table.get(doc_id=search)['term']

    async def plot_and_send(self, search, fig, channel):
        """Make the plot and send it to discord."""
        filename = 'tracka/dbs/pandas/{}.png'.format(search)
        plotly.plotly.image.save_as(fig, filename=filename, height=1080, width=1920)
        with open(filename, 'rb') as f:
            await self.client.send_file(channel, f)

    async def retrieve_ccxt_data(self, coin):
        """Get data from binance. Return only close price and timestamp."""
        resample_freq = '24H'
        lookback_days = 7
        # This has to match the google trends lookback days. Works only with 7 days for now based on
        # google trends api limitations
        binance = ccxt.binance()
        binance.load_markets()
        since = datetime.datetime.now() - datetime.timedelta(days=lookback_days)
        since = since.timestamp() * 1000

        bin_ohlcv = (binance.fetch_ohlcv(coin, '1h', since=int(since)))
        bin_ohlcv = pd.DataFrame(bin_ohlcv, columns=['Date', 'Open', 'High', 'Low', 'Close', 'Volume']).set_index(('Date'))
        bin_ohlcv.index = pd.to_datetime(bin_ohlcv.index, unit='ms')
        bin_ohlcv = bin_ohlcv.resample(resample_freq).mean()

        timestamp = bin_ohlcv.index
        closep = bin_ohlcv['Close']
        binance.close()
        return timestamp, closep

    async def return_percent(self, search_term):
        """Does Nothing."""
        time_frames = ['now 1-d', 'now 7-d', 'today 1-m']  # , 'now 1-H', 'now 4-H'
        string = ''
        for j in time_frames:
            filename = 'tracka/dbs/pandas/' + search_term + '_' + j + '.db'
            with open(filename, 'rb') as fp:
                string += pickle.load(fp)
        return string


def setup(client):
    """ Import trends class to the discord commands. """
    trn = TrendsCommand(client)
    com1 = discord.ext.commands.Command(name='add', callback=trn.add, pass_context=True)
    trnd = discord.ext.commands.Group(name='gt', callback=trn.g, pass_context=True)
    client.add_command(trnd)
    trnd.add_command(com1)
    client.add_cog(TrendsCommand(client))
