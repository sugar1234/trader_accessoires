# coding=utf-8
"""Detect candle formation for all CDL functions of talib."""
import datetime
import random

import ccxt
import configFiles.config as config
import discord
import pandas as pd
import plotly
import talib
from autologging import traced, logged
from helpers.utility import BaseFunction


@traced
@logged
class CandleFormationFunction(BaseFunction):
    """Detect candle formation for all CDL functions of talib."""

    def __init__(self):
        self.binance = ccxt.bitmex()
        self.binance.load_markets()

        plotly.tools.set_credentials_file(username='swilso793', api_key='OjKiOPJdUisoF7AUO443')
        self.coin = 'BTC/USD'

    async def main(self, lookback, ccxt_time, pandas_time):
        """Main. Will expand later."""
        # %TODO look above
        since = datetime.datetime.now() - datetime.timedelta(days=lookback[0], hours=lookback[1], weeks=lookback[2])
        since = since.timestamp() * 1000
        bin_ohlcv = self.binance.fetch_ohlcv(self.coin, ccxt_time, since=int(since))
        bin_ohlcv = pd.DataFrame(bin_ohlcv, columns=['Date', 'Open', 'High', 'Low', 'Close', 'Volume']).set_index(('Date'))
        bin_ohlcv.index = pd.to_datetime(bin_ohlcv.index, unit='ms')
        bin_ohlcv = bin_ohlcv.resample(pandas_time).mean()
        # timestamp = bin_ohlcv.index
        closep = bin_ohlcv['Close'].astype(float)
        lowp = bin_ohlcv['Low'].astype(float)
        highp = bin_ohlcv['High'].astype(float)
        openp = bin_ohlcv['Open'].astype(float)
        # price = go.Scatter(x=timestamp, y=closep, yaxis='y2', name='price')
        flag = False
        channel = discord.Object(id=config.discord_channel_ids['candle_formation'])
        payload = discord.Embed(colour=random.randint(0, 16777215), title=pandas_time)
        for cdl in talib.get_function_groups()['Pattern Recognition']:
            cdl_func = getattr(talib, cdl)
            result = cdl_func(openp.values, highp.values, lowp.values, closep.values)
            self.__log.info(result)
            if result[-1] == 100:
                payload.add_field(name=cdl[3:], value='Bearish signal triggered.')
                flag = True
            elif result[-1] == -100:
                payload.add_field(name=cdl[3:], value='Bulish signal triggered.')
                flag = True

        if flag:
            await self.client.send_message(channel, embed=payload)
