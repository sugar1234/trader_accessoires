#!/bin/bash
export PYTHONPATH="${PYTHONPATH}:`pwd`/tracka"
# Want to debug asyncio?
# export PYTHONASYNCIODEBUG=1
python3 tracka/main.py
