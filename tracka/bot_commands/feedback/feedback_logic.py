# coding=utf-8
import csv
import datetime

from tracka.item_classes.feedback import Feedback, FeedbackAlert


class FeedbackLogic:
    """ Handles the feedback logic. Useful for testing"""

    def __init__(self, client=None):
        self.client = client

    @staticmethod
    def set_alert_fun(start, increment, author_id, server_id):
        """

        :param server_id:   The server id that need the feedback.
        :param start:       The first time to notify you if notification reach this amount.
        :param increment:   Every increment times after start you get notified as well.
        :param author_id:   The id of the author which chose this alert.
        :return:            Tuple (start, increment).
        """
        if increment == 0:
            increment = 10000
        if increment is None:
            increment = start
        if increment:
            increment = int(increment)
        FeedbackAlert.objects(author_id=author_id, server_id=server_id).update_one(set__start=start, set__increment=increment, upsert=True)
        return start, increment

    @staticmethod
    def log_all(message, aid, sid, name):
        """Save everything in database."""
        is_anonymous = False
        if '-anonymous' in message.lower():
            is_anonymous = True
        text = message.replace('?feedback add', '').replace('-anonymous', '').strip()
        if text:
            feedback = Feedback(is_anonymous=is_anonymous, author_id=aid, content=text, time=datetime.datetime.now(), server_id=sid, author_name=name)
            feedback.save()
            return 1
        else:
            return 0

    @staticmethod
    def format_feedback(data, name):
        """Format target for pushing."""
        text = ''
        count = 0
        sent_some_message = False
        list_of_feedback = []
        for i in data:
            count += 1
            if i.is_anonymous:
                text += '\n\nAnonymous:\n'
            else:
                text += '\n\n{}\n'.format(name)

            text += i.content
            if len(text) > 400 or count == data.count():
                list_of_feedback.append('```\n' + text + '\n```')
                sent_some_message = True
                text = ''
        if not sent_some_message:
            return ['Nothing found.']
        data.update(is_seen=True)
        return list_of_feedback

    @staticmethod
    def query_database(server_id, is_new=False, is_anonymous=False, is_all=False):
        """Ask the database for data."""
        data = False
        if is_all:
            if is_new:
                data = Feedback.objects(is_seen=False, server_id=server_id)
            else:
                data = Feedback.objects(server_id=server_id)
        elif is_anonymous:
            if is_new:
                data = Feedback.objects(is_seen=False, is_anonymous=True, server_id=server_id)
            else:
                data = Feedback.objects(is_anonymous=True, server_id=server_id)
        elif not is_anonymous:
            if is_new:
                data = Feedback.objects(is_seen=False, is_anonymous=False, server_id=server_id)
            else:
                data = Feedback.objects(is_anonymous=False, server_id=server_id)
        return data

    @staticmethod
    def export_logic(guild_id, filename):
        """Used for exporting to csv."""
        data = Feedback.objects(server_id=guild_id)
        list_of_posts = list()
        for i in data:
            if i.is_anonymous:
                list_of_posts.append(['Anonymous', i.time, i.content])
            else:
                list_of_posts.append([i.author_name, i.time, i.content])
        with open(filename, "w") as output:
            writer = csv.writer(output, lineterminator='\n')
            writer.writerow(['Name', 'Time', 'Content'])
            writer.writerows(list_of_posts)
