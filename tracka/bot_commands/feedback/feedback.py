# coding=utf-8
"""Feedback."""

import discord
from discord.ext import commands

import tracka.configFiles.config as config
from tracka.bot_commands.feedback.feedback_logic import FeedbackLogic
from tracka.helpers import checks
from tracka.item_classes.feedback import Feedback, FeedbackAlert


class FeedbackCommand:
    """Import Feedback Command."""

    def __init__(self, client):
        self.client = client
        self.logic = FeedbackLogic(client)

    @staticmethod
    async def feedback(ctx):
        """ Main command. Doesn't do anything by itself."""
        if ctx.invoked_subcommand is None:
            await ctx.send('Try using `?help feedback`.')

    async def break_up_messages(self, ctx, data):
        """ BReaks up the messages to chunks the discord api can handle."""
        items = self.logic.format_feedback(data, ctx.author.name)
        for item in items:
            await ctx.send(item)

    async def notification(self, server_id):
        """Send notification if conditions apply"""
        data = Feedback.objects(is_seen=False)
        for user_id in config.can_read_feedback:
            user_prefs = FeedbackAlert.objects(author_id=user_id, server_id=server_id).first()
            if user_prefs:
                if data.count() % user_prefs.increment == 0 or data.count() == user_prefs.start:
                    await self.client.get_user(user_id).send('You have {} unread feedback messages.'.format(data.count()))
            else:
                FeedbackAlert.objects(author_id=user_id, server_id=server_id).update_one(set__start=20, set__increment=20, upsert=True)

    @staticmethod
    def pred(m):
        """ Used to wait for message in add command."""
        return m.author.name != config.bot_name

    async def add(self, ctx):
        """
        Description:
            Use this command to leave feedback.

        Examples::

            ?feedback add I want to have more cat gif's pronto.
            ?feedback add #anime I think we should have a channel to discuss onepiece.

        Tips:
            * You can even specify a tag with a #. (Example 2)

        Note:
            By typing the command in a private message with the Bot your feedback will be tagged anonymous.

        """
        if isinstance(ctx.channel, discord.DMChannel):
            text = '```'
            if len(self.client.guilds) == 1:
                guild_id = self.client.guilds[0].id
                out_message = ctx.message.content + '-anonymous'
            else:
                await ctx.send('What server would you like to leave feedback for?')
                for i in self.client.guilds:
                    text += f'{i.name}, {i.id}\n'
                await ctx.send(f'{text}```')
                await ctx.send('Please copy and paste the server id you want to use.')
                message = await self.client.wait_for('message', check=self.pred)
                guild_id = int(message.content)
                out_message = ctx.message.content + '-anonymous'
        else:
            guild_id = ctx.guild.id
            out_message = ctx.message.content
        if self.logic.log_all(out_message, ctx.author.id, guild_id, ctx.author.name):
            await ctx.send('Accepted.')
            await self.notification(guild_id)
        else:
            await ctx.send("Feedback can't be blank.")

    @commands.check(checks.can_read_feedback)
    @commands.check(checks.if_no_pm)
    async def get_all(self, ctx):
        """
        Description:
            Print public and anonymous feedback in one list.

        Examples::

            ?feedback get_all -new        -- Return feedback since last time you checked.
            ?feedback get_all             -- Return all feedback since start of time.
        """
        data = self.logic.query_database(ctx.guild.id, '-new' in ctx.message.content, is_all=True)
        await self.break_up_messages(ctx, data)

    @commands.check(checks.can_read_feedback)
    @commands.check(checks.if_no_pm)
    async def set_alert(self, ctx, start: int, increment=None):
        """
        Description:
            Set an alert period.

        Args:
            start:       After how many unread feedback messages do you want to be alerted for the first time.
            increment:   After how many unread feedback messages do you want to be alerted for each time after that.

        Examples::

            ?set_alert 20 10     -- Alerted when there are 20 messages and every 10 messages after that.
            ?set_alert 20 0      -- Alerted only when there are 20 messages once.
            ?set_alert 20        -- Alerted every 20 messages.

        """
        self.logic.set_alert_fun(start, increment, ctx.author.id, ctx.guild.id)
        await ctx.send('Alert entered.')

    @commands.check(checks.if_no_pm)
    @commands.check(checks.can_read_feedback)
    async def get_anon(self, ctx):
        """
        Description:
            Print anonymous feedback in one list.

        Examples::

            ?feedback get_anon -new        -- Return anonymous feedback since the last time you checked.
            ?feedback get_anon             -- Return all anonymous feedback since start of time.

        """
        data = self.logic.query_database(ctx.guild.id, '-new' in ctx.message.content, is_anonymous=True)
        await self.break_up_messages(ctx, data)

    @commands.check(checks.can_read_feedback)
    @commands.check(checks.if_no_pm)
    async def delete_all(self, ctx):
        """
        Example::

            ?feedback delete_all

       Warning:
           This will delete everything so be careful.

        """
        Feedback.objects().delete()
        await ctx.send('I hope you did the right thing.')

    @commands.check(checks.can_read_feedback)
    @commands.check(checks.if_no_pm)
    async def get_public(self, ctx):
        """
        Description:
            Print public feedback in one list.

        Examples::

            ?feedback get_public -new        -- Return feedback since last time you checked.
            ?feedback get_public             -- Return all public feedback since start of time.

        """
        data = self.logic.query_database(ctx.guild.id, '-new' in ctx.message.content, is_anonymous=False)
        await self.break_up_messages(ctx, data)

    @commands.check(checks.can_read_feedback)
    @commands.check(checks.if_no_pm)
    async def export(self, ctx):
        """
        Description:
            Export feedback data to csv, with author/anonymous, time and content columns.

        Examples::

            ?feedback export

        """
        filename = 'feedback.csv'
        self.logic.export_logic(ctx.guild.id, filename)
        try:
            await ctx.author.send(f"{ctx.guild.name}'s feedback.", file=discord.File(filename))
            await ctx.send("Check your PM's.")
        except discord.errors.Forbidden:
            await ctx.send(f"{ctx.guild.name}'s feedback.", file=discord.File(filename))


def setup(client):
    """Add the class to the discord command pool"""
    feed = FeedbackCommand(client)
    main = commands.Group(name='feedback', callback=feed.feedback, case_insensitive=True)
    getal = commands.Command(name='get_all', callback=feed.get_all, case_insensitive=True, aliases=['getall', 'all', 'get-all'])
    add = commands.Command(name='add', callback=feed.add, case_insensitive=True,  pass_context=True)
    getan = commands.Command(name='get_anon', callback=feed.get_anon, case_insensitive=True,  pass_context=True, aliases=['getanon', 'get-anon'])
    getexp = commands.Command(name='export', callback=feed.export, case_insensitive=True,  pass_context=True, aliases=['tocsv', 'csv', 'to_csv', 'to-csv'])
    setal = commands.Command(name='set_alert', callback=feed.set_alert, case_insensitive=True,  pass_context=True, aliases=['setalert', 'alert', 'set-alert'])
    delal = commands.Command(name='delete_all', callback=feed.delete_all, case_insensitive=True,  pass_context=True, aliases=['delete-all', 'delall', 'del_all', 'del-all'])
    getfc = commands.Command(name='get_public', callback=feed.get_public, case_insensitive=True,  pass_context=True, aliases=['get-public', 'getpublic', 'get-pub', 'getpub', 'get_pub'])
    client.add_command(main)
    main.add_command(add)
    main.add_command(getan)
    main.add_command(getal)
    main.add_command(getexp)
    main.add_command(setal)
    main.add_command(delal)
    main.add_command(getfc)
    client.add_cog(FeedbackCommand(client))
