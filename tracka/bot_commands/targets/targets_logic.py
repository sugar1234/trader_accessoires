# coding=utf-8
import json
import math
import random

import discord
import mongoengine as me
import texttable as tt
from autologging import logged, traced

from tracka.item_classes import exchange as ex
from tracka.item_classes.target import Target
from tracka.item_classes.user_preferences import UserPref


@traced
@logged
class TargetsCommand:
    """Base target functions."""

    def add(self, author_id, author_name, name: str, prices: str, dist, exchange, is_public, server_id=None):
        """Handle adding to the lists."""
        cmc_tickers = json.loads(ex.Exchange.objects(name='coinmarketcap').get().tickers)
        if '$' in name or '/' not in name:
            name = name.replace('/', '').replace('$', '') + '/USD'
        name = name.upper()
        base = name.split('/')[0]
        quote = name.split('/')[1]
        base = self.fix_coinmarketcap_pairs(base)
        try:
            try:
                exchange_ticker = json.loads(ex.Exchange.objects(name=exchange).get().tickers)
            except me.errors.DoesNotExist:
                return f'Exchange {exchange} is not currently supported.'
            current_price = exchange_ticker[name]['last']
        except KeyError:
            try:
                current_price = cmc_tickers[name]['last']
            except KeyError:
                current_price = cmc_tickers[f'{base}/USD']['last'] / cmc_tickers[f'{quote}/USD']['last']
        if exchange != 'coinmarketcap':
            try:
                exchange_ticker[name]
            except KeyError:
                return f"Exchange {exchange} does not offer this pair. Did you use the correct symbol?"

        for price_in_quote, comment in zip(prices.split(','), dist.split(',')):
            # Determine direction of target
            is_above = current_price < float(price_in_quote)
            try:
                if server_id:
                    message_target = server_id
                else:
                    message_target = author_id
                Target(ticker=name, author_id=author_id, author_name=author_name, quote=quote, base=base, is_public=is_public,
                       target_price=price_in_quote, starting_price=current_price, is_hit=False, type=comment,
                       is_above=is_above, server_id=server_id, exchange=exchange, message_target=message_target).save()
            except me.errors.NotUniqueError as e:
                self.__log.debug(e, exc_info=True)
                return f"Please add a unique comment. {comment} exists already for this pair."
        return "Added {} with target price of {} {}!".format(name, ", ".join([i for i in prices.split(',')]), quote)

    @staticmethod
    def fix_coinmarketcap_pairs(base):
        """Add here all the pairs that coinmarketcap lists differently."""
        dict_of_coins = {"BQX":"ETHOS"}
        if base in dict_of_coins:
            cmc_tickers = ex.Exchange.objects(name='coinmarketcap').get().tickers
            tickers = cmc_tickers.replace(base, dict_of_coins[base])
            ex.Exchange.objects(name='coinmarketcap').update_one(tickers=tickers, upsert=True)
            return dict_of_coins[base]
        return base

    def get_info(self, data, is_compact):
        """Get the info from coinmarketcap for presenting the target."""
        cmc_tickers = json.loads(ex.Exchange.objects(name='coinmarketcap').get().tickers)
        compact_list = list()
        payloads = list()
        if not data:
            return 'Nothing found. Try `?help`'
        distinct_tickers = data.distinct(field='ticker')
        for item in distinct_tickers:
            pipeline = [{"$match": {'ticker': item}}]
            results = list(data.aggregate(*pipeline))
            target = results[0]
            base = self.fix_coinmarketcap_pairs(target['base'])
            # If it's in usd
            try:
                quote_price = float(cmc_tickers[target['quote'] + '/USD']['info']['price_usd'])
            except KeyError:
                quote_price = 1
            kaka = dict()
            kaka['prices'] = list()
            kaka['type'] = list()
            # Iterate over every result that has the same ticker.
            for result in results:
                if not result['is_hit']:
                    kaka['prices'].append(result['target_price'] * quote_price)
                    kaka['prices'].append(result['target_price'])
                    kaka['type'].append(result['type'])
            pass_values = ['price_usd', 'price_btc', '24h_volume_usd', 'percent_change_1h', 'percent_change_24h', 'percent_change_7d']
            for value in pass_values:
                kaka[value] = float(cmc_tickers[f'{base}/USD']['info'][value])
            kaka['quote_price_usd'] = quote_price
            kaka['exchanges'] = data.distinct(field='exchange')
            if not is_compact:
                payloads.append(self.format_target(kaka, target['ticker']))
            else:
                compact_list.extend(self.format_compact_inner_loop(kaka, target['ticker']))

        if is_compact and compact_list:
            return self.format_compact(compact_list)
        else:
            return payloads

    @staticmethod
    def format_compact(compact_list):
        """Format the compact info based on compact list."""
        tab = tt.Texttable()
        tab.set_cols_width([8, 8, 8, 14, 8, 8, 8, 14])  # , 7, 8, 5, 12])
        for cnt in range(0, 1024, 8):
            if len(compact_list) >= cnt + 8:
                tab.add_row(compact_list[cnt:cnt + 8])
            elif len(compact_list) > cnt:
                dif = (8 - (len(compact_list) - cnt))
                push = compact_list[cnt:]
                push.extend(['-' for _ in range(dif)])
                tab.add_row(push)
            else:
                break
        return '```{}```'.format(tab.draw())

    def format_compact_inner_loop(self, table, name):
        price_usd = '{}$ '.format(self.rd(table['price_usd']))
        changeh = 'H: {:.1f}% '.format(table['percent_change_1h'])
        volume = '{:,}$'.format(int(table['24h_volume_usd']))
        return [name, price_usd, changeh, volume]

    @staticmethod
    def rd(number):
        """Round the number to the first 3 non decimals."""
        log = 1 - int(math.log(number, 10))
        return '{:.9f}'.format(round(number, log + 2)).rstrip('0').rstrip('.')

    def format_target(self, table, name):
        """Format target in a table style."""
        cmc_tickers = json.loads(ex.Exchange.objects(name='coinmarketcap')[0].tickers)
        payload = discord.Embed(title=name, colour=random.randint(0, 16777215))
        pair = name.split(' ---')[0]
        names = ['Target  ', 'CMC', 'Percent Change', 'CMC', 'Type']
        # Format percent changes from CMC
        tab = tt.Texttable()
        tab.header(['Hour', 'Day', 'Week'])
        tab.add_row([str(table['percent_change_1h']) + '%', str(table['percent_change_24h']) + '%', str(table['percent_change_7d']) + '%'])
        s = tab.draw() + '\n'
        payload.add_field(name='Change', value='```\n' + s + '\n```', inline=True)

        # Format Volume
        tab = tt.Texttable()
        tab.set_cols_align(['r', 'r'])
        tab.header(['Exchange', 'USD'])
        tab.add_row([names[3], '{:,}'.format(int(table['24h_volume_usd']))])
        tickers = {}
        for exchange in table['exchanges']:
            if exchange == 'coinmarketcap':
                continue
            tickers[exchange] = json.loads(ex.Exchange.objects(name=exchange).get().tickers)
        for key, value in tickers.items():
            quote_volume = value[pair]['quoteVolume']
            if quote_volume is None:
                volume = int(self.rd(float(value[pair]['baseVolume']) * table['price_usd']))
            else:
                volume = int(self.rd(float(quote_volume) * table['quote_price_usd']))
            tab.add_row([key, '{:,}'.format(volume)])
        s = tab.draw() + '\n'
        payload.add_field(name='Volume', value='```\n' + s + '\n```', inline=True)

        # Format Targets
        tab = tt.Texttable()
        quote = pair.split('/')[1]
        if quote == 'USD':
            tab.header(['Comm.', quote])
            tab.set_cols_dtype(['t', 't'])
            for i in range(0, len(table['prices']), 2):
                tab.add_row([table['type'][i // 2], '{}'.format(self.rd(table['prices'][i + 1]))])
        else:
            tab.header(['Comment', 'USD', quote])
            tab.set_cols_dtype(['t', 't', 't'])
            for i in range(0, len(table['prices']), 2):
                print('-------')
                print(table['type'])
                print(table['prices'])
                print('----')
                tab.add_row([table['type'][i // 2], '{}'.format(self.rd(table['prices'][i])),
                             '{}'.format(self.rd(table['prices'][i + 1]))])
        s = tab.draw() + '\n'
        payload.add_field(name='Targets', value='```\n' + s + '\n```', inline=True)

        # Format Exchanges
        tab = tt.Texttable()
        if quote == 'USD':
            tab.set_cols_dtype(['t', 't', 't'])
            tab.header(['Name', 'USD', 'BTC'])
            tab.add_row([names[1], '{}'.format(self.rd(table['price_usd'])), '{}'.format(self.rd(table['price_btc']))])
        else:
            tab.set_cols_dtype(['t', 't', 't'])
            tab.header(['Name', 'USD', pair.split('/')[1]])
            tab.add_row([names[1], '{}'.format(self.rd(table['price_usd'])),
                         '{}'.format(self.rd(table['price_usd'] / table['quote_price_usd']))])
            for key, value in tickers.items():
                price_in_pair = value[pair]['last']
                tab.add_row([key, self.rd(price_in_pair * cmc_tickers[quote + '/USD']['last']), '{}'.format(self.rd(price_in_pair))])
        s = tab.draw() + '\n'
        payload.add_field(name='Exchanges', value='```\n' + s + '\n```', inline=True)
        return payload

    @staticmethod
    def remove(coin, message_target, target_comment, is_public):
        """Handle removing from the lists."""
        if not coin:
            Target.objects(message_target=message_target, is_public=is_public).delete()
            return "Your list is now history."
        coin = coin.upper()
        if '/' in coin and target_comment:
            Target.objects(message_target=message_target, ticker=coin, type=target_comment).delete()
            return f"Deleted {coin} target with comment {target_comment}"
        elif target_comment:
            return "Please specify a pair to delete a comment from."
        if coin[0] == '/':
            Target.objects(is_public=is_public, message_target=message_target, quote=coin[1:]).delete()
            return "Deleted all pairs of x{}.".format(coin.upper())
        elif coin[-1] == '/':
            Target.objects(is_public=is_public, message_target=message_target, base=coin[:-1]).delete()
            return "Deleted all pairs of {}x.".format(coin.upper())
        elif '/' in coin:
            Target.objects(is_public=is_public, message_target=message_target, ticker=coin).delete()
            return "Deleted all pairs of {}.".format(coin.upper())
        elif coin:
            Target.objects(is_public=is_public, message_target=message_target, base=coin).delete()
            return "Deleted all pairs of {}/x.".format(coin.upper())

    @staticmethod
    def shower(coin, message_target, is_public):
        """Return filtered data."""
        coin = coin.upper()
        if coin[0] == '/':
            data = Target.objects(is_public=is_public, message_target=message_target, quote=coin[1:])
        elif coin[-1] == '/':
            data = Target.objects(is_public=is_public, message_target=message_target, base=coin[:-1])
        elif '/' in coin:
            data = Target.objects(is_public=is_public, message_target=message_target, ticker=coin)
        else:
            data = Target.objects(is_public=is_public, message_target=message_target, base=coin)
        return data

    @staticmethod
    def set_alert(author_id, author_name, value, is_public):
        """Set alert preferences."""
        if value not in ['on', 'off']:
            return 'Please use on or off as values.'
        if value == 'on':
            value = True
        elif value == 'off':
            value = False
        if is_public:
            UserPref.objects(author_id=author_id, author_name=author_name).update_one(t_alerts=value, upsert=True)
            return 'Alert option for public targets set successfully.'

        else:
            UserPref.objects(author_id=author_id, author_name=author_name).update(pv_alerts=value, upsert=True)
            return 'Alert option for private targets set successfully.'
