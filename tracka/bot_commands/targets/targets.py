# coding=utf-8
"""Calculate targets."""
import discord
import discord.ext.commands.context
from discord.ext import commands

from tracka.bot_commands.targets.targets_logic import TargetsCommand
from tracka.helpers import checks
from tracka.item_classes import target as tr


class PrivateTarget:
    """Handle ?pv command."""

    def __init__(self, client):
        """Import Target Commands."""
        self.client = client
        self.tcmd = TargetsCommand()

    async def pv(self, ctx):
        """
        Example::

            ?pv                  -- Compact list with all your private targets.

        Note:

            If you are in a public channel a confirmation message will appear to make sure you want to show your private list.

        """
        if ctx.invoked_subcommand is None:
            data = tr.Target.objects(is_public=False, author_id=ctx.message.author.id)
            await self.check_if_should_show(ctx)
            data = self.tcmd.get_info(data, True)
            await self.push_to_discord(ctx, data)

    async def push_to_discord(self, ctx, data):
        if isinstance(data, str):
            await ctx.send(data)
        if isinstance(data, list):
            for payload in data:
                await ctx.send('', embed=payload)

    async def check_if_should_show(self, ctx):
        if isinstance(ctx.channel, discord.abc.GuildChannel):
            await ctx.send('Press 1 to confirm action. Showing *private* targets to *public* channel')

            def pred(m):
                """ Used to wait for message in add command."""
                return ctx.author == m.author

            msg = await self.client.wait_for('message', check=pred)

            if msg.content != '1':
                await ctx.send('Abort.')
                return 0
            return 1

    async def remove(self, ctx, coin: str, target_comment=''):
        """
        Examples::

            ?pv remove ada           -- Will remove all pairs that have ada as a base ticker.
            ?pv remove ada/          -- Functionally same as the above.
            ?pv remove XRP/btc       -- Will remove XRP/BTC pair.
            ?pv remove /XRP          -- Will remove all pairs that have XRP as their quote.
            ?pv remove eth/BTC targ1 -- Will remove the specific target with comment targ1 from ETH/BTC pair.

        Note:

            This is non reversible.
        """
        text = self.tcmd.remove(coin, ctx.message.author.id, target_comment, False)
        await ctx.send(text)

    async def show(self, ctx, coin: str):
        """
        Examples::

            ?pv show xrp         -- Will show all your private XRP/x pairs in detail.
            ?pv show xrp/        -- Will show all your private XRP/x pairs in detail. Same as above
            ?pv show /btc        -- Will show all your private x/BTC pairs in detail.
            ?pv show xrp/btc     -- Will show only the private pair XRP/BTC in detail.

        Args:

            coin:   Can be a ticker or a pair. See Examples.

        Returns:

            * Target Prices along with comments
            * Volume and Actual Price on each exchange you added
            * Coinmarketcap's percent changes.

        Note:

            You can filter for base, quote tickers or whole pairs.

        """
        data = self.tcmd.shower(coin, ctx.message.author.id, False)
        await self.check_if_should_show(ctx)
        data = self.tcmd.get_info(data, False)
        await self.push_to_discord(ctx, data)

    async def deleteAll(self, ctx):
        """
        Example::

            ?pv deleteAll        -- Will remove ALL your private targets.

        Note:

            This is not reversible. Be careful
        """
        text = self.tcmd.remove('', ctx.message.author.id, '', False)
        await ctx.send(text)

    async def add(self, ctx, pair: str, prices: str, comment: str, exchanges="coinmarketcap"):
        """
        Examples::

            ?pv add bnb/btc 0.4 short bitfinex
            ?pv add ada/eth 0.2 long binance
            ?pv add eth/usd 1000 bull            -- For usd pairs you don't need an exchange
            ?pv add eth$ 1000 bear               -- Functionally same as above

        Args:
            pair:        The pair you want to add.
            prices:      The target price you want to save.
            comment:     A comment that will serve as a unique identifier.
            exchanges:   The exchange you want to check prices from.

        Note:

            You can add multiple targets to every pair. Each target must have it's own comment.
            Adding an exchange is optional although highly recommended if you want to user target alerts. It defaults to coinmarketcap.

        """
        output = self.tcmd.add(ctx.author.id, ctx.author.name, pair, prices, comment, exchanges, False)
        await ctx.send(output)

    async def setAlert(self, ctx, value):
        """
        Examples::

            ?pv setAlert on           -- Enable notifications for triggered targets
            ?pv setAlert off          -- Disable notifications for triggered targets

        """
        text = self.tcmd.set_alert(ctx.author.id, ctx.author.name, value, False)
        await ctx.send(text)


class PublicTarget:
    """Handle ?t commands."""

    def __init__(self, client):
        """Import Target Commands."""
        self.client = client
        self.tcmd = TargetsCommand()
    async def push_to_discord(self, ctx, data):
        if isinstance(data, str):
            await ctx.send(data)
        if isinstance(data, list):
            for payload in data:
                await ctx.send('', embed=payload)

    async def t(self, ctx):
        """
        t()
        :Example:

        .. code-block:: python

            ?t                  -- Compact list with all your public targets.

        """
        if ctx.invoked_subcommand is None:
            data = tr.Target.objects(is_public=True, author_id=ctx.message.author.id)
            data = self.tcmd.get_info(data, True)
            await self.push_to_discord(ctx, data)


    async def remove(self, ctx, coin: str, target_comment=""):
        """
        remove(coin, target_comment)

        :Example:

        .. code-block:: python

            ?t remove ada           -- Will remove all pairs that have ada as a base ticker.
            ?t remove ada/          -- Functionally same as the above.
            ?t remove XRP/btc       -- Will remove XRP/BTC pair.
            ?t remove /XRP          -- Will remove all pairs that have XRP as their quote.
            ?t remove eth/BTC targ1 -- Will remove the specific target with comment targ1 from ETH/BTC pair.

        .. note::

            This is non reversible.
        """
        text = self.tcmd.remove(coin, ctx.message.author.id, target_comment, True)
        await ctx.send(text)

    async def show(self, ctx, coin: str):
        """
        show(coin)

        :Examples:

        .. code-block:: python

            ?pv show xrp         -- Will show all your public XRP/x pairs in detail.
            ?pv show xrp/        -- Will show all your public XRP/x pairs in detail. Same as above
            ?pv show /btc        -- Will show all your public x/BTC pairs in detail.
            ?pv show xrp/btc     -- Will show only the public pair XRP/BTC in detail.

        :Parameter:

            * Can be a ticker or a pair. See Examples.

        :Returns:

            * Target Prices along with comments
            * Volume and Actual Price on each exchange you added
            * Coinmarketcap's percent changes.

        .. note::

            You can filter for base, quote tickers or whole pairs.

        """
        data = self.tcmd.shower(coin, ctx.message.author.id, True)
        data = self.tcmd.get_info(data, False)
        await self.push_to_discord(ctx, data)

    async def deleteAll(self, ctx):
        """
        deleteAll()

        :Examples:

        .. code-block:: python

            ?t deleteAll        -- Will remove ALL your public targets.

        .. note::

            This is not reversible. Be careful
        """
        text = self.tcmd.remove('', ctx.message.author.id, '', True)
        await ctx.send(text)

    async def add(self, ctx, pair: str, prices: str, comment: str, exchanges="coinmarketcap"):
        """
        add(pair, price, comment, exchange)

        :Examples:

        .. code-block:: python

            ?t add bnb/btc 0.4 short bitfinex
            ?t add ada/eth 0.2 long binance
            ?t add eth/usd 1000 bull            -- For usd pairs you don't need an exchange
            ?t add eth$ 1000 bear               -- Functionally same as above

        :param pair:        The pair you want to add.
        :param prices:      The target price you want to save.
        :param comment:     A comment that will serve as a unique identifier.
        :param exchanges:   The exchange you want to check prices from.

        .. note::

            You can add multiple targets to every pair. Each target must have it's own comment.
            Adding an exchange is optional. It defaults to coinmarketcap.

        """
        text = self.tcmd.add(ctx.message.author.id, ctx.message.author.name, pair, prices, comment, exchanges, True)
        await ctx.send(text)

    async def setAlert(self, ctx, value):
        """
        setAlert(value)

        :Examples:

        .. code-block:: python

            ?t setAlert on           -- Enable notifications for triggered targets
            ?t setAlert off          -- Disable notifications for triggered targets

        """
        text = self.tcmd.set_alert(ctx.message.author.id, ctx.message.author.name, value, True)
        await ctx.send(text)


class PublicOneList:
    """Handle ?pub commands."""
    async def push_to_discord(self, ctx, data):
        if isinstance(data, str):
            await ctx.send(data)
        if isinstance(data, list):
            for payload in data:
                await ctx.send('', embed=payload)

    def __init__(self, client):
        """Import Target Commands."""
        self.client = client
        self.tcmd = TargetsCommand()

    async def pub(self, ctx):
        """
        pub()
        :Example:

        .. code-block:: python

            ?pub                  -- Compact list with all your server's public targets.

        """
        if ctx.invoked_subcommand is None:
            data = tr.Target.objects(is_public=True, server_id=ctx.guild.id)
            data = self.tcmd.get_info(data, True)
            await self.push_to_discord(ctx, data)
        # elif ctx.message.server is None:
        # await self.client.say('Please use a server channel to add to your server list.')

    @commands.check(checks.use_pub_command)
    async def remove(self, ctx, coin: str, target_comment=""):
        """
        remove(coin, target_comment)

        :Example:

        .. code-block:: python

            ?pub remove ada           -- Will remove all pairs that have ada as a base ticker.
            ?pub remove ada/          -- Functionally same as the above.
            ?pub remove XRP/btc       -- Will remove XRP/BTC pair.
            ?pub remove /XRP          -- Will remove all pairs that have XRP as their quote.
            ?pub remove eth/BTC targ1 -- Will remove the specific target with comment targ1 from ETH/BTC pair.

        .. note::

            This is non reversible.
        """
        text = self.tcmd.remove(coin, ctx.guild.id, target_comment, True)
        await ctx.send(text)

    async def show(self, ctx, coin: str):
        """
        show(coin)

        :Examples:

        .. code-block:: python

            ?pv show xrp         -- Will show all your servers public XRP/x pairs in detail.
            ?pv show xrp/        -- Will show all your servers public XRP/x pairs in detail. Same as above
            ?pv show /btc        -- Will show all your servers public x/BTC pairs in detail.
            ?pv show xrp/btc     -- Will show only the servers public pair XRP/BTC in detail.

        :Parameter:

            * Can be a ticker or a pair. See Examples.

        :Returns:

            * Target Prices along with comments
            * Volume and Actual Price on each exchange you added
            * Coinmarketcap's percent changes.

        .. note::

            You can filter for base, quote tickers or whole pairs.

        """
        data = self.tcmd.shower(coin, ctx.guild.id, True)
        data = self.tcmd.get_info(data, False)
        await self.push_to_discord(ctx, data)

    @commands.check(checks.use_pub_command)
    async def deleteAll(self, ctx):
        """
        deleteAll()

        :Examples:

        .. code-block:: python

            ?pub deleteAll        -- Will remove ALL your server's public targets.

        .. note::

            This is not reversible. Be careful
        """
        text = self.tcmd.remove('', ctx.guild.id, '', True)
        await ctx.send(text)

    @commands.check(checks.use_pub_command)
    async def add(self, ctx, pair: str, prices: str, comment: str, exchanges="coinmarketcap"):
        """
        add(pair, price, comment, exchange)

        :Examples:

        .. code-block:: python

            ?t add bnb/btc 0.4 short bitfinex
            ?t add ada/eth 0.2 long binance
            ?t add eth/usd 1000 bull            -- For usd pairs you don't need an exchange
            ?t add eth$ 1000 bear               -- Functionally same as above

        :param pair:        The pair you want to add.
        :param prices:      The target price you want to save.
        :param comment:     A comment that will serve as a unique identifier.
        :param exchanges:   The exchange you want to check prices from.

        .. note::

            You can add multiple targets to every pair. Each target must have it's own comment.
            Adding an exchange is optional. It defaults to coinmarketcap.

        """
        text = self.tcmd.add(ctx.author.id, ctx.author.name, pair, prices, comment, exchanges, True, ctx.guild.id)
        await ctx.send(text)


def setup(client):
    """ Import these classes to the discord commands."""
    # Register private commands
    prt = PrivateTarget(client)
    prtrm = discord.ext.commands.Command(name='remove', callback=prt.remove, pass_context=True)
    prtsh = discord.ext.commands.Command(name='show', callback=prt.show, pass_context=True)
    prtda = discord.ext.commands.Command(name='deleteAll', callback=prt.deleteAll, pass_context=True)
    prtad = discord.ext.commands.Command(name='add', callback=prt.add, pass_context=True)
    prtsa = discord.ext.commands.Command(name='setAlert', callback=prt.setAlert, pass_context=True)
    send = discord.ext.commands.Group(name='pv', callback=prt.pv, pass_context=True)
    client.add_command(send)
    send.add_command(prtrm)
    send.add_command(prtsh)
    send.add_command(prtda)
    send.add_command(prtad)
    send.add_command(prtsa)

    # Register public commands
    pbt = PublicTarget(client)
    prtrm = discord.ext.commands.Command(name='remove', callback=pbt.remove, pass_context=True)
    prtsh = discord.ext.commands.Command(name='show', callback=pbt.show, pass_context=True)
    prtda = discord.ext.commands.Command(name='deleteAll', callback=pbt.deleteAll, pass_context=True)
    prtad = discord.ext.commands.Command(name='add', callback=pbt.add, pass_context=True)
    prtsa = discord.ext.commands.Command(name='setAlert', callback=pbt.setAlert, pass_context=True)
    send = discord.ext.commands.Group(name='t', callback=pbt.t, pass_context=True)
    client.add_command(send)
    send.add_command(prtrm)
    send.add_command(prtsh)
    send.add_command(prtda)
    send.add_command(prtad)
    send.add_command(prtsa)

    # Register one true list
    pbt = PublicOneList(client)
    prtrm = discord.ext.commands.Command(name='remove', callback=pbt.remove, pass_context=True)
    prtsh = discord.ext.commands.Command(name='show', callback=pbt.show, pass_context=True)
    prtda = discord.ext.commands.Command(name='deleteAll', callback=pbt.deleteAll, pass_context=True)
    prtad = discord.ext.commands.Command(name='add', callback=pbt.add, pass_context=True)
    send = discord.ext.commands.Group(name='pub', callback=pbt.pub, pass_context=True)
    client.add_command(send)
    send.add_command(prtrm)
    send.add_command(prtsh)
    send.add_command(prtda)
    send.add_command(prtad)
    client.add_cog(PrivateTarget(client))
    client.add_cog(PublicOneList(client))
    client.add_cog(PublicTarget(client))
