# coding=utf-8
"""ANNOUNCE."""
import random

import discord
from discord.ext import commands

from tracka.configFiles import config
from tracka.helpers import checks


class AnnounceCommand:
    """Global Administrator commands."""

    def __init__(self, client):
        """Introduce all client event handlers to the main event loop."""
        self.allowed_ids = config.allowed_ids
        self.allowed_server_ids = config.allowed_server_ids
        self.client = client

    @commands.check(checks.boss_and_private)
    async def send(self, ctx, title: str, *content):
        """
        Examples::

            ?send "Bitcoin" Saturday is burn day.
            ?send "Bitcoin Day" You have to gather at exactly 6 pm. Till then.
            ?send "title" Message.

        Description:
            * Make an announcement, sending a nicely formatted personal message to all members of the server.


        Args:
            title: The title you want to supply to your announcement. Enclose it in double quotes ".
            content: The content of the message. Can be anything.

        Note:
            This is particularly useful in case of members that have muted the @everyone, @all tags.

        """
        message = ctx.message
        description = " ".join(content)
        author = message.author.name.split('#')[0].capitalize()
        payload = discord.Embed(colour=random.randint(
            0, 16777215), title=title, description=description)
        payload.set_author(name=author)
        for server in self.client.servers:
            if server.id in self.allowed_server_ids:
                for i in server.members:
                    try:
                        await self.client.send_message(i, embed=payload)
                    except discord.errors.HTTPException:
                        pass

    @commands.check(checks.can_kick)
    async def kick_group(self, ctx, in_group, flag='all', *out_groups):
        """
        Examples::

            ?kickGroup new                    -- Will kick everyone with role new.
            ?kickGroup new notIn pro          -- Will kick everyone with role new that hasn't the role pro.
            ?kickGroup new notIn pro master   -- Will kick everyone with role new that hasn't the roles pro and master.
            ?kickGroup "toiletpaper's"        -- Will kick everyone with role toiletpaper's.

        Description:
            * Kick members based on the roles they have and on the roles they don't have.

        Args:
            in_group: The group you want the members to be kicked from.
            flag: Use notIn if you want to specify more groups. If the members belong in these groups as well they wont be kicked.
            out_groups: The groups you want the members not to belong in.

        Tips:
            * If there are any weird characters in the role name enclose it in " "
            * If you supply only an in_group (Example 1) it will delete **all** members in this role, irrespective of any other role they belong to.

        Note:
            A confirmation message will appear showing you all members about to be kicked.

        """
        to_be_kicked = []
        for server in self.client.servers:
            if server.id in self.allowed_server_ids:
                for i in server.members:
                    role_names = []
                    for j in i.roles:
                        role_names.append(j.name)
                    out_flag = False
                    if in_group in role_names:
                        if flag.lower() == 'notin':
                            for out in out_groups:
                                if out in role_names:
                                    print('name -->', i.name)
                                    out_flag = True
                        if not out_flag:
                            to_be_kicked.append(i)
        text = '```Are you sure you want to delete these dogs?[yes/no]\nUsernames found: {}```'
        text = text.format(" ".join(
            ['\n\t-- {}'.format(i.name) for i in to_be_kicked]))
        await self.confirmer(ctx, to_be_kicked, text)

    @commands.check(checks.can_kick)
    async def kick_group_adv(self, ctx, *groups):
        """
        Examples::

            ?kickGroupAdv new_members         -- Will kick members that have only the role new_members.
            ?kickGroupAdv new_members oldies  -- Will kick members that have only the roles new_members and oldies.
            ?kickGroupAdv "toiletpaper's"     -- Will kick members that have only the role toiletpaper's.

        Description:

            * Kick members based on their roles by combining the groups they are in.

        Args:
            groups: The groups you want the members to be kicked from. See Example 2.

        Tips:
            * If there are any weird characters in the role name enclose it in double quotes ". (Example 3).
            * Will delete members that are **only** in the groups that you provided.

        Note:
            A confirmation message will appear showing you all members about to be kicked.

        """
        to_be_kicked = []
        for server in self.client.servers:
            if server.id in self.allowed_server_ids:
                for i in server.members:
                    role_names = []
                    for j in i.roles:
                        role_names.append(j.name)
                    if len(role_names) == len(groups) and set(role_names) == set(groups):
                        to_be_kicked.append(i)
        text = '```Are you sure you want delete all dogs in role(s) {}?[yes/no]\nUsernames found: {}```'
        text = text.format(groups, " ".join(
            ['\n\t-- {}'.format(i.name) for i in to_be_kicked]))
        await self.confirmer(ctx, to_be_kicked, text)

    async def confirmer(self, ctx, to_be_kicked, text):
        """Issues the confirmation message and acts accordingly."""
        await self.client.say(text)
        msg = await self.client.wait_for_message(author=ctx.message.author)
        if msg.content != 'yes':
            await self.client.say('Abort.')
            return
        if msg.content == 'yes':
            for i in to_be_kicked:
                await self.client.kick(i)
            await self.client.say('ADIOS.')


def setup(client):
    """Sets up the magic."""
    anc = AnnounceCommand(client)
    send = discord.ext.commands.Command('send', callback=anc.send, pass_context=True)
    client.add_command(send)
    kick_g = discord.ext.commands.Command('kick_group', callback=anc.kick_group, pass_context=True, aliases=['kickGroup', 'kick-group'])
    client.add_command(kick_g)
    kick_ga = discord.ext.commands.Command('kick_group_adv', callback=anc.kick_group_adv, pass_context=True, aliases=['kickGroupAdv' 'kick-group-adv'])
    client.add_command(kick_ga)
    client.add_cog(AnnounceCommand(client))
