# coding=utf-8
import glob
import os

import mongoengine as me
from discord.ext import commands

from tracka.item_classes.server_preferences import ServerPrefs


class ServerPreferences:
    """Import Feedback Command."""

    def __init__(self, client):
        self.client = client

    @staticmethod
    async def func(ctx):
        """ Main command. Doesn't do anything by itself."""

        if ctx.invoked_subcommand is None:
            await ctx.send('Try using `?help func`.')
        else:
            print(ctx.invoked_subcommand)

    @staticmethod
    async def list_them(ctx):
        """
        Description:
            * Show all available functions.

        Examples::

            ?func list
        """
        bot_functions = ([os.path.splitext(os.path.basename(x))[0] for x in glob.glob("./tracka/functions/*.py") if 'init' not in x and 'loadexchange' not in x])
        await ctx.send(bot_functions)

    @staticmethod
    async def add(ctx, func_name, extra=''):
        """

        Description:
            * add the specified function to your server.

        Args:
            func_name: The name of the function you want to enable. All functions can be listed with the help of the `?func list` command.
            extra: This is only useful for twitter tracking one person in their own channel
        Examples::

            ?func add omniwallet
            ?func add twitterfol @aantonop
        """
        if extra:
            channel_name = f'{func_name}_{extra}'
        else:
            channel_name = func_name
        channel = await ctx.guild.create_text_channel(channel_name)
        try:
            ServerPrefs(server_id=ctx.guild.id, function_name=func_name, channel_id=channel.id, extra=extra).save()
        except me.NotUniqueError:
            await channel.delete()
            await ctx.send('Function is already enabled.')

    async def delete(self, ctx, func_name, extra=''):
        """Remove an enabled function from the server."""
        if extra:
            func_name = f'{func_name}_{extra}'
        try:
            item = ServerPrefs.objects(server_id=ctx.guild.id, function_name=func_name, extra=extra).get()
        except me.DoesNotExist:
            await ctx.send("This function doesn't exist or isn't enabled for this server.")
            return
        try:
            await self.client.get_channel(item.channel_id).delete()
        except AttributeError:
            pass
        item.delete()
        await ctx.send(f'{func_name} deleted from this server.')


def setup(client):
    """ Needed by discord to import this cog."""
    sp = ServerPreferences(client)
    base = commands.Group(name='func', callback=sp.func, pass_context=True)
    list_them = commands.Command('list_them', callback=sp.list_them, pass_context=True, aliases=['list'])
    add_them = commands.Command(name='add', callback=sp.add, pass_context=True)
    del_them = commands.Command(name='delete', callback=sp.delete, pass_context=True)
    client.add_command(base)
    base.add_command(list_them)
    base.add_command(add_them)
    base.add_command(del_them)
    client.add_cog(sp)
