"""Test for configuration errrors."""
import configFiles.constants as constants


def test_twitter_accounts():
    """Test if starting with @."""
    for i in constants.twitter_accounts:
        assert '@' in i[0]
        assert isinstance(i, str)


def test_twitter_all_communication():
    """Test if starting with @ and is string."""
    for i in constants.twitter_person_all_communication:
        assert '@' in i[0]
        assert isinstance(i, str)


def test_whitelist():
    """Test if lower case."""
    for i in constants.whitelist:
        assert i == i.lower()
        assert isinstance(i, str)


def test_blacklist():
    """Test if lowercase."""
    for i in constants.blacklist:
        assert i == i.lower()
        assert isinstance(i, str)


def test_arbitrage_symbols():
    """Test if upper and not too long."""
    for i in constants.symbols:
        assert 1 <= len(i) < 6
        assert i == i.upper()
        assert isinstance(i, str)


def test_min_volume_in_usd():
    """Test if volume is right."""
    assert isinstance(constants.min_volume_in_usd, float)
    assert constants.min_volume_in_usd > 0


def test_arbitrage_percentage():
    """Test if float and sane number."""
    assert isinstance(constants.percent_difference, float)
    assert 0.0 < constants.percent_difference < 35.0


def test_blacklist_exchange():
    """Test if all strings and lowercase."""
    for i in constants.blacklist_exch:
        assert i == i.lower()
        assert isinstance(i, str)


def test_omniwallets():
    """Test for omniwallet."""
    for i, v in constants.omniwallets.items():
        assert isinstance(i, str)
        assert isinstance(v, str)

    for i, v in constants.omniwallet_ids_names.items():
        assert isinstance(i, str)
        assert isinstance(v, int)
        assert v > 0
