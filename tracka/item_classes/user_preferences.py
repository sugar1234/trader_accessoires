# coding=utf-8
import mongoengine as me


class UserPref(me.Document):
    """Define user preferences."""
    author_id = me.IntField()
    author_name = me.StringField()
    pv_alerts = me.BooleanField(default=True)
    t_alerts = me.BooleanField(default=True)
