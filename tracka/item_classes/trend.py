# coding=utf-8
# coding=utf-8
import mongoengine as me


class Search(me.Document):
    """Define one search object."""
    search_term = me.StringField(unique_with='time_frame')
    time_frame = me.StringField()
    last_updated = me.DateTimeField()
    values = me.ListField()
    date_values = me.ListField(me.DateTimeField())
    percent_change = me.FloatField()
    slope = me.FloatField()


class Trend(me.Document):
    """Define the trend object."""

    author_name = me.StringField()
    author_id = me.StringField()
    search_terms = me.ListField(me.ReferenceField(Search))
    # time_frames = me.ListField(default=['now 1-d', 'now 7-d', 'today 1-m'])  # , 'now 1-H', 'now 4-H'
