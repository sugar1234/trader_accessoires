"""Exchange object."""
import mongoengine as me


class Inner(me.EmbeddedDocument):
    symbol = me.StringField()
    vwap = me.FloatField()
    low = me.FloatField()
    last = me.FloatField()
    open = me.FloatField()
    ask = me.FloatField()
    timestamp = me.IntField()
    bid = me.FloatField()
    close = me.FloatField()
    baseVolume = me.FloatField()
    change = me.FloatField()
    first = me.FloatField()
    info = me.DictField()
    high = me.FloatField()
    average = me.FloatField()
    datetime = me.DateTimeField()
    percentage = me.FloatField()
    quoteVolume = me.FloatField()


class Exchange(me.Document):
    """ Define one exchange object."""
    name = me.StringField()
    symbols = me.ListField(me.StringField())
    tickers = me.StringField()
    last_updated = me.DateTimeField()
    # candles5m = me.ListField()


class InnerCandle(me.EmbeddedDocument):
    """Define one candle."""
    date = me.DateTimeField()
    open = me.FloatField()
    high = me.FloatField()
    low = me.FloatField()
    close = me.FloatField()
    volume = me.FloatField()


class Candle(me.Document):
    """ Define one candle object."""
    ticker = me.StringField()
    time_frame = me.StringField()
    exchange = me.StringField()
    values = me.EmbeddedDocumentListField(InnerCandle)
