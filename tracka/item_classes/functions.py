# coding=utf-8
import mongoengine as me


class Functions(me.Document):
    """ Create omniwallet database table."""
    server_id = me.IntField(unique_with=['data_id'])
    data_id = me.StringField()
    function_name = me.StringField()
    function_prefs = me.DictField()
