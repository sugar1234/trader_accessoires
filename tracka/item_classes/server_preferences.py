# coding=utf-8
import mongoengine as me


class ServerPrefs(me.Document):
    """Define user preferences."""
    server_id = me.IntField(unique_with=['function_name', 'extra'])
    function_name = me.StringField()
    channel_id = me.IntField()
    extra = me.StringField(default='')
