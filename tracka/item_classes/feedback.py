# coding=utf-8
"""Template for feedback."""
import mongoengine as me


class Feedback(me.Document):
    """Define the feedback object."""

    is_anonymous = me.BooleanField()
    content = me.StringField()
    author_id = me.IntField()
    author_name = me.StringField()
    time = me.DateTimeField()
    is_seen_by = me.ListField(me.StringField())
    is_seen = me.BooleanField(default=False)
    server_id = me.IntField()


class FeedbackAlert(me.Document):
    """Holds the alert settings."""
    author_id = me.IntField(unique=True)
    start = me.IntField()
    increment = me.IntField()
    server_id = me.IntField()
