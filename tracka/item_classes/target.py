"""Calculate targets."""
from mongoengine import Document, FloatField, StringField, BooleanField, DateTimeField, IntField


class Target(Document):
    """Define the target object."""

    author_name = StringField()
    message_target = IntField(unique_with=('ticker', 'is_public', 'type'))
    author_id = IntField()
    server_id = IntField()
    is_public = BooleanField()
    exchange = StringField()
    ticker = StringField()
    base = StringField()
    quote = StringField()
    unique_name = StringField()
    target_price = FloatField()
    starting_price = FloatField()
    is_hit = BooleanField(default=False)
    date_hit = DateTimeField()
    type = StringField()
    is_above = BooleanField()
