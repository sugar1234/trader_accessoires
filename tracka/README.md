# Track some things.

## To use configuration: 
  * `cp example_config.py config.py`
  * `nano config.py`
  * Enter your api keys.
## Tracked stuff.
* [Twitter accounts](/tracka/constants.py)
* [Keywords Whitelist/Blacklist](/tracka/constants.py)
* [Historical data](/tracka/dbs/)
* [Coinmarketcal](https://coinmarketcal.com)
* [New listings on binance](/tracka/constants.py)
## Discord 
* [Channels and token](/tracka/example_config.py)
## Sentry
* [Token](/tracka/example_config.py)
## Twitter
* [Various tokens and secrets](/tracka/example_config.py)

## Python dependencies
* `pip3 install --user -r back_req_for_future_use.txt --upgrade`




#?feedback command 

Commands for users:

?feedback -anonymous  blablabla                                    # the -anonymous is optional and only if u want the feedback to be anonymous the -anonymous flag can be random in the text somwhere
?feedback balblabla                  # stores the feedback in the database 


Command for admin
?feedback getAll             # gets all the database as textmessage if u dm the bot 
You can add the flag      -new         to get only the one you didn t fetch / read   yet for example you write: 
?feedback getAll -new         ( the new flag works on all admin commands as an option)

?feedback getAn               # gets all the anonymous data list if you put here the -new flag it gets only the anonymous new messasges
?feedback getFace          # gets only the one that are not anonymous       you can add also here the flag -new for example ?feedback getFace -new 

#The Bot will notify you when the storage is filled with x new messages that you can set and you can also set with y the alert on increments f.e.
 
?feedback setAlert x y 
#when x messages are reached it gives u an alert that it reached x messages in the database. Y is optional and is the variable for the increments. So if you put    ?feedback setAlert 20 10         it will notify you for the first time after 20 messages got reached and then each time its +10 on top of that 20 
if you let the last argument blank ( the y)  and write ?feedback setAlert 20          it will notify you for the first time on 20 messages and then each 20 messages again          and if u write ?feedback setAlert 20 0       it will notify u oinly on the 20 messages and not again even if the storage gets filled with more messages. If u then read them  it will start from 0 and notify on 20 again


Standart is 20 for x 0 for y
