# coding=utf-8
"""THIS IS A MONSTER."""
import glob
import logging.config
import os

import discord

import tracka.configFiles.config as config
import tracka.functions.arbitrage
import tracka.functions.listings
import tracka.functions.loadexchange
import tracka.functions.omniwallet
import tracka.functions.targetalerts
import tracka.functions.twitterfol
from tracka.setup import Starter


async def initialize_bot_function(name):
    """ Abstraction to load bot functions. Used by setup_sched"""
    obj = getattr(getattr(tracka.functions, name), name.capitalize())()
    if obj:
        logging.error(f'Starting {name}.')
        await obj.main()
    else:
        logging.debug(f'{name} didnt load as a function.')


def setup_sched():
    """Perform setup for scheduled tasks."""
    bot_functions = ([os.path.splitext(os.path.basename(x))[0] for x in glob.glob("./tracka/functions/*.py")])
    for bot_function in bot_functions:
        Starter.scheduler.add_job(initialize_bot_function, 'cron', [bot_function], minute='*/3', jitter=120)
        # Starter.scheduler.add_job(initialize_bot_function, 'interval', [bot_function], seconds=30)


def setup_bot_commands():
    """ Perform setup for async bot commands."""
    bot_command_modules = ([x.replace('./tracka/', '').replace('/', '.').replace('.py', '') for x in glob.glob("./tracka/bot_commands/**/*.py", recursive=True)])
    for extension in bot_command_modules:
        try:
            client.load_extension(extension)
        except (discord.ClientException, ModuleNotFoundError):
            logging.debug(f'Module {extension} misses some required functions.')


if __name__ == '__main__':
    client = Starter.client
    setup_sched()
    setup_bot_commands()
    Starter.scheduler.start()
    client.run(config.discord_config['token'])
