# coding=utf-8
import asyncio
import datetime
import json
import time

import ccxt.async_support as ccxt
from autologging import logged, traced

from tracka.configFiles import constants
from tracka.item_classes.exchange import Exchange, Candle
from tracka.item_classes.target import Target


@logged
@traced
class Loadexchange():
    """Load exchange tickers, symbols and candles."""

    def __init__(self, loop=None):
        self.constants = constants
        if loop:
            self.loop=loop
        else:
            self.loop = asyncio.get_event_loop()


    async def main(self):
        """Main function to load exchange data."""
        # connect('test', host='mongo')
        tasks = []
        # Retrieve ticker data for the not blacklisted exchanges defined in constants.
        for exchange_name in ccxt.exchanges:
            if exchange_name not in self.constants.blacklist_exch:
                exch = getattr(ccxt, exchange_name)()
                asyncio.sleep(1)
                tasks.append(self.inner(exch, exchange_name))
        await asyncio.ensure_future(asyncio.gather(*tasks), loop=self.loop)

        # Get candles for all exchanges defined in someones targets
        try:
            await self.candles()
        except Exception:
            self.__log.error('Some kind of error with the candles. Could be nothing.')

    async def inner(self, exch, name):
        """Run the symbol, ticker snatching."""
        try:
            await exch.load_markets()
            symbols = exch.symbols
            if exch.has['fetchTickers']:
                tickers = await exch.fetch_tickers()
                updated = Exchange.objects(name=name).update(tickers=json.dumps(tickers), symbols=symbols, last_updated=datetime.datetime.now())
                if not updated:
                    exchange = Exchange(name=name, tickers=json.dumps(tickers), symbols=symbols, last_updated=datetime.datetime.now())
                    exchange.save()
        except Exception:
            self.__log.warning(f'{name} failed to connect.')

        await exch.close()

    async def candles(self):
        """Run the candle snatching every one minute."""
        exchanges = list()
        tickers = list()
        try:
            for i in Target.objects():
                try:
                    exchanges.extend(i.exchanges)
                    tickers.append(f'{i.ticker_name}/{i.quote}')
                except Exception as e:
                    self.__log.info(e)
        except Exception as e:
            self.__log.error(e)

        now = datetime.datetime.now()
        then = datetime.timedelta(hours=1)
        exchanges = (list(set(exchanges)))
        tickers = (list(set(tickers)))
        tasks = []
        for exch in exchanges:
            exch_obj = getattr(ccxt, exch)()
            for ticker in tickers:
                await self.inner_loop(exch, exch_obj, now, then, ticker)

            await exch_obj.close()
        await asyncio.ensure_future(asyncio.gather(*tasks), loop=self.loop)

    async def inner_loop(self, exch, exch_obj, now, then, ticker):
        """Run the inner loop of candle snatching."""
        try:
            candle_data = await exch_obj.fetch_ohlcv(ticker, timeframe='5m', since=int(time.mktime((now - then).timetuple()) * 1000))
        except Exception:
            self.__log.warning(f'{exch} failed to load ohlcv for {ticker}')
            candle_data = []
        candle_data = [{'date': datetime.datetime.fromtimestamp(j[0] / 1000), 'open': j[1], 'high': j[2], 'low': j[3], 'close': j[4], 'volume': j[5]} for j in [i for i in candle_data]]
        if not Candle.objects(ticker=ticker, time_frame='5m', exchange=exch).update(values=candle_data) and candle_data:
            candle = Candle(ticker=ticker, time_frame='5m', exchange=exch, values=candle_data)
            candle.save()
