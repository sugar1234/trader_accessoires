"""Handle target alerting to discord."""

import json
import uuid
from datetime import datetime

from autologging import logged, traced

# Local Imports
from tracka.helpers.utility import BaseFunction
from tracka.item_classes.exchange import Exchange, Candle
from tracka.item_classes.server_preferences import ServerPrefs
from tracka.item_classes.target import Target
from tracka.item_classes.user_preferences import UserPref


@traced
@logged
class Targetalerts(BaseFunction):
    """Handle target alerting to discord."""

    async def main(self):
        """Handle target alerting."""
        all_targets = Target.objects()
        for target in all_targets:
            exch_ticker = Exchange.objects(name=target.exchange).get().tickers
            try:
                var = json.loads(exch_ticker)[target.ticker]
                current_price = var['last']
                candles = Candle.objects(ticker=target.ticker, time_frame='5m', exchange=target.exchange)
                # If no candle is found. eg. on coinmarketcap it goes by last price
                if candles.count() == 0 and target.is_hit is False:
                    if (target.is_above and current_price >= target.target_price) or (not target.is_above and current_price <= target.target_price):
                        await self.change_db_and_send(target)
                # If a candle on an exchange is found it check if it's between high and low
                elif target.is_hit is False:
                    last_candle = candles.get().values[-1]
                    if last_candle['high'] >= target.target_price >= last_candle['low'] and target.is_hit is False:
                        await self.change_db_and_send(target)
            except KeyError as e:
                self.__log.debug(e, exc_info=True)
                continue

    async def change_db_and_send(self, target):
        """Change the relevant entries and push the target notification."""
        Target.objects(message_target=target.message_target, is_hit=False,
                       type=target.type).update_one(is_hit=True,
                                                    date_hit=datetime.now(), type__=f'{target.type}_hit_{str(uuid.uuid4())}', upsert=True)
        await self.check_and_notify(target)

    async def check_and_notify(self, target):
        """Checks if the user wants to be notified and does just that."""
        user = self.client.get_user(target.author_id)
        # (check.user_from_id(self.client, [whole_target.author_id]))
        user_pref = UserPref.objects(author_id=target.author_id)
        content_server = f"{target.author_name}'s **{target.ticker}**, **{target.type}**@**{target.target_price}** triggered."
        content = f"YOUR **{target.ticker}**, **{target.type}**@**{target.target_price}**."
        # send to the public channel.
        if target.server_id:
            for item in ServerPrefs.objects(function_name='targetalerts'):
                await self.sender(self.client.get_channel(item.channel_id), content_server)
        # If the user has not specified any preferences.
        if user_pref.count() == 0:
            await self.sender(user, content)
            return
        # Send to user private or public channels.
        if target.is_public:
            if user_pref.get().t_alerts:
                await self.sender(user, content)
        else:
            if user_pref.get().pv_alerts:
                await self.sender(user, content)

    @staticmethod
    async def sender(target, content):
        """ Take over sending to discord."""
        await target.send(content)

    @staticmethod
    def pd(a, b):
        """Calculate percent difference."""
        max_e = max(a, b)
        min_e = min(a, b)
        return ((max_e - min_e) / min_e) * 100
