# coding=utf-8
"""Handle Twitter."""
import asyncio
import json

from autologging import traced, logged

from tracka.helpers.exc_dec import exception
from tracka.helpers.utility import BaseFunction
# Local Imports
from tracka.item_classes.server_preferences import ServerPrefs


@traced
@logged
class Twitterfol(BaseFunction):
    """Handle Twitter."""

    def __init__(self):
        self.twitter_accounts = self.constants.twitter_accounts
        # self.twitter_persons = self.constants.twitter_person_all_communication
        # self.table = self.db.table('twitter_post')

    @exception()
    async def main(self):
        """Look through twitter accounts for specific keywords and push to discord."""
        tasks = []
        for handle in self.twitter_accounts:
            tasks.append(self.twitter_post_inner(handle))
        for item in ServerPrefs.objects(extra__ne=''):
            # extra holds the handle of the user
            tasks.append(self.twitter_track_all(item.extra))
        await asyncio.gather(*tasks)
        self.__log.info('Twitter done without any errors.')
        return 1

    @exception()
    async def twitter_track_all(self, handle):
        """Handle tracking all communication that comes from a single user."""
        self.__log.debug(f'Twitter bot handling {handle}.')
        statuses = self.api.GetUserTimeline(screen_name=handle, count=10, include_rts=True, trim_user=True, exclude_replies=False)
        for s in statuses:
            s = json.loads(str(s))
            post_id = (s['id'])
            link = 'https://twitter.com/{}/status/{}'.format(handle, post_id)
            handle = handle.replace('@', '')
            text = '**{} tweeted:**\n{}'.format(handle.capitalize(), link)
            try:
                in_reply_to = s['in_reply_to_screen_name']
                text = '**{} replied to {}**\n{}'.format(handle.capitalize(), in_reply_to.capitalize(), link)
            except KeyError:
                try:
                    retweet = s['user_mentions'][0]['screen_name']
                    if retweet not in handle:
                        text = "**{} retweeted {}'s tweet.**\n{}".format(handle.capitalize(), retweet.capitalize(), link)
                except (KeyError, IndexError):
                    pass
            await self.push({'id': link, 'text': text}, 'twitterfol', extra=f'@{handle}')

    @exception()
    async def twitter_post_inner(self, handle):
        """Handle each iteration."""
        statuses = self.api.GetUserTimeline(screen_name=handle, count=5, include_rts=False, trim_user=True, exclude_replies=True)
        for s in statuses:
            s = json.loads(str(s))
            post_id = (s['id'])
            link = 'https://twitter.com/{}/status/{}'.format(handle, post_id)
            text = s['full_text']
            if self.interesting(text):
                await self.push({'id': link, 'text': link}, 'twitterfol')
            await asyncio.sleep(3)
