# coding=utf-8
"""Handle omniwallet api."""
import asyncio
import json
import random

import discord
import humanfriendly
from autologging import logged, traced

# Local Imports
from tracka.helpers.exc_dec import exception
from tracka.helpers.utility import BaseFunction


@logged
@traced
class Omniwallet(BaseFunction):
    """Handle omniwallet api."""

    async def omniwallet_inner(self, name, wallet):
        """Track transactions with omniwallet."""
        # %TODO make it work with more than the last transaction
        data = await self.requests_error_handling('http://omniexplorer.info/ask.aspx?api=gethistory&address={}'.format(wallet))
        data = json.loads(data)
        first_tx = (data['transactions'][0])
        tdata = await self.requests_error_handling('http://omniexplorer.info/ask.aspx?api=gettx&txid={}'.format(first_tx))
        tdata = json.loads("{" + tdata + "}")
        confirms = tdata['confirmations']
        check = first_tx + '\tC: ' + str(confirms)

        description = "**Amount:** *{}$*\n**Tx** *{}*\n**Confirmations:** *{}/6*".format(
            humanfriendly.format_number(tdata['amount']), first_tx, str(confirms))
        payload = discord.Embed(title=name, description=description, colour=random.randint(0, 16777215))
        text = '**New Transaction for {}.**'.format(name)
        if confirms <= 6 & confirms % 2 == 0:
            await self.push({'id': check, 'text': text}, 'omniwallet', payload)

    @exception()
    async def tether_total_supply(self):
        """Gets total supply change shenanigans."""
        tether_id = 31
        name = 'tether'
        data = await self.requests_error_handling('http://omniexplorer.info/ask.aspx?api=getpropertytotaltokens&prop={}'.format(tether_id))
        if data:
            amount = humanfriendly.format_number(float(data))
            text = '**Total Supply for {} changed to: {}$**'.format(name, amount)
            payload = discord.Embed(title=text, colour=random.randint(0, 16777215))
            await self.push({'id': data, 'text': ''}, 'omniwallet', payload)

    @exception()
    async def main(self):
        """Iterate over the addresses."""
        try:
            tasks = []
            for name, wallet in self.constants.omniwallets.items():
                tasks.append(self.omniwallet_inner(name, wallet))
            tasks.append(self.tether_total_supply())
            await asyncio.gather(*tasks)
            self.__log.info('Omniwallet finished without any errors.')
        except Exception:
            self.__log.info('Omniwallet failed.')
