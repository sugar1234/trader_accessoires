# coding=utf-8
"""Handle new binance listings."""
import asyncio
import random
from datetime import datetime

import discord
from autologging import traced, logged

from tracka.configFiles.constants import listing_exchanges
from tracka.helpers.exc_dec import exception
# Local Imports
from tracka.helpers.utility import BaseFunction
from tracka.item_classes.exchange import Exchange


@traced
@logged
class Listings(BaseFunction):
    """Handle new listings."""

    def __init__(self):
        self.table = self.db.table('listings')

    # %TODO adapt this to make private notifications based on private coins.
    @exception()
    async def main(self):
        """Report new listings in your coins."""
        tasks = []
        for exchange in listing_exchanges:
            for symbol in Exchange.objects(name=exchange)[0].symbols:
                title = f"**{exchange.capitalize()} lists {symbol}.**"
                url = f'https://www.{exchange}.com'
                payload = discord.Embed(title=title, url=url,
                                        timestamp=datetime.now(), colour=random.randint(0, 16777215))
                text = f'**New Listing on {exchange.capitalize()}**'
                tasks.append(self.push({'id': f'{symbol}, {exchange}', 'text': text}, 'listings', payload))
        await asyncio.gather(*tasks)
        self.__log.info('Listings done without any errors.')
        return 1
