# coding=utf-8
"""Attempt taking a shot at the beast."""
import asyncio
import json
import random
import re

import ccxt.async_support as ccxt
import cfscrape
import discord
import texttable as tt
from autologging import logged, traced

import tracka.configFiles.constants as constants
from tracka.helpers.exc_dec import exception
from tracka.helpers.utility import BaseFunction
from tracka.item_classes import exchange as ex


@logged
@traced
class Arbitrage(BaseFunction):
    """Handle arbitrage detection, comparison and notification."""

    def __init__(self):
        self.stuff = dict()
        for i in constants.symbols:
            self.stuff[i] = {'asks': list(), 'bids': list()}

        self.cmc_tickers = json.loads(ex.Exchange.objects(name='coinmarketcap')[0].tickers)
        self.pairs = []
        self.table = self.db.table('arbitrage')

    async def main(self):
        """Arbitrage detection."""
        pairs = await self.requests_error_handling('https://min-api.cryptocompare.com/data/all/exchanges')
        self.__log.info('Arbitrage started.')
        self.get_pairs(pairs)
        tasks = []

        # Look through exchanges from within ccxt
        for name in ccxt.exchanges:
            self.__log.debug('Name: {}'.format(name))
            if not isinstance(name, str) or name not in self.constants.whitelist_exch:
                continue

            excha = getattr(ccxt, name)()
            tasks.append(self.arbitrage_inner_loop(name, excha))

        await asyncio.gather(*tasks)
        self.__log.info('Tasks gathered.')

        # Looks through exchanges that are unsupported by ccxt
        for symbol in constants.symbols:
            await self.extra_exchanges(symbol)
        tasks = []
        for pair in self.stuff:
            tasks.append(self.arbitrage_push(pair))
        await asyncio.gather(*tasks)
        self.__log.info('Arbitrage Round Done.')

    @exception()
    def aggregate(self, base_volume, quote_volume, symbol, splitter, ask_quote, bid_quote, link, name):
        """Get all relevant data and put it into the database for comparing."""
        try:
            base_symbol = symbol.split(splitter)[0]
            quote_symbol = symbol.split(splitter)[1]
            base_usd = float(self.cmc_tickers[base_symbol + '/USD']['info']['price_usd'])
            quote_usd = float(self.cmc_tickers[quote_symbol + '/USD']['info']['price_usd'])
        except KeyError:
            self.__log.debug('Something missing for: {}'.format(name))
            return

        # Necessary hack to determine volume. Some times one of them is empty.
        if quote_volume is None:
            volume_usd = float(base_volume) * base_usd
        else:
            volume_usd = float(quote_volume) * quote_usd
        if volume_usd < constants.min_volume_in_usd:
            self.__log.debug("{}, {}".format(volume_usd, name))
            return
        self.stuff[base_symbol]['asks'].append((float(ask_quote) * quote_usd, quote_symbol, name, volume_usd, ask_quote))
        self.stuff[base_symbol]['bids'].append((float(bid_quote) * quote_usd, quote_symbol, name, volume_usd, bid_quote))

    @exception()
    async def arbitrage_inner_loop(self, name, excha):
        """Check for all the possible errors and loads market/ticker."""
        try:
            await excha.load_markets()
            for pair in self.pairs:
                if pair in excha.symbols:
                    data = await excha.fetch_ticker(pair)
                    self.aggregate(data['baseVolume'], data['quoteVolume'], data['symbol'], '/', float(data['ask']), float(data['bid']), excha.urls['www'], name)
            await excha.close()
        except Exception:
            await excha.close()

    @exception()
    async def arbitrage_push(self, pair):
        """Pushes results to discord."""
        self.__log.info('Arbitrage stuff for {}\n{}'.format(pair, self.stuff[pair]))
        try:
            max_bid = max(self.stuff[pair]['bids'])
            min_ask = min(self.stuff[pair]['asks'])
            percent_difference = ((max_bid[0] - min_ask[0]) / min_ask[0]) * 100
        except Exception:
            return
        self.__log.info('Arbitrage for {} is {}'.format(pair, percent_difference))
        if percent_difference > constants.percent_difference:
            tab = tt.Texttable()
            tab.header(['', 'Buy Here', 'Sell Here'])
            tab.set_precision(9)

            tab.add_row(['USD Price', '{}'.format(min_ask[0]), '{}'.format(max_bid[0])])
            tab.add_row(['Pair Price', '{}'.format(min_ask[4]), '{}'.format(max_bid[4])])
            tab.add_row(['Pair', '{}'.format(min_ask[1]), '{}'.format(max_bid[1])])
            tab.add_row(['Market', '{}'.format(min_ask[2]), '{}'.format(max_bid[2])])
            tab.add_row(['Volume', '{:.2f}$'.format(min_ask[3]), '{:.2f}$'.format(max_bid[3])])
            tab.set_deco(tt.Texttable.VLINES | tt.Texttable.HEADER)
            sa = tab.draw() + '\n'
            description = '```' + sa + '```'
            payload = discord.Embed(description=description, colour=random.randint(0, 16777215))
            text = '**{}: {:.2f}%**'.format(pair, percent_difference)
            check = pair + str(percent_difference)
            print(text)
            await self.push({'id': check, 'text': text}, 'arbitrage', payload)

    @exception()
    async def extra_exchanges(self, symbol):
        """MERCATOX, BANCOR."""

        # Only used for KIN
        async def handle_merca():
            if symbol != "KIN":
                return
            mercatox = json.loads(await self.requests_error_handling('https://mercatox.com/public/json24'))['pairs']
            scraper = cfscrape.create_scraper()
            for key in mercatox:
                if symbol == key.split('_')[0]:
                    data = mercatox[key]
                    page = scraper.get('https://mercatox.com/exchange/{}/{}'.format(symbol, key.split('_')[1])).content
                    match = re.search('Highest Bid.*ask">(\d+.\d+)', page.decode('utf-8'))
                    highestBid = match.group(1)
                    match = re.search('Lowest Ask.*ask">(\d+.\d+)', page.decode('utf-8'))
                    lowestAsk = match.group(1)
                    self.aggregate(data['baseVolume'], data['quoteVolume'], key, '_', lowestAsk, highestBid, 'https://mercatox.com', 'mercatox')

        async def handle_bancor():
            """Handle bancor api."""
            bancor = json.loads(await self.requests_error_handling('https://api.bancor.network/0.1/currencies/{}/ticker?fromCurrencyCode=BNT'.format(symbol)))
            if not bancor:
                self.__log.info('Bancor failed to load.')
                return
            try:
                data = bancor['data']
                volume = float(data['volume24h']) * (10 ** (-1 * data['decimals']))
                self.aggregate(None, volume, symbol + '_BNT', '_', data['price'], data['price'], 'https://bancor.network', 'bancor')
            except KeyError:
                self.__log.debug('Bancor has no data for {}.'.format(symbol))

        await handle_bancor()

    def get_pairs(self, data):
        """Get all the pairs of a currency from cryptocompare."""
        symbols = constants.symbols
        # Finds all pairs for a given symbol
        for symbol in symbols:
            temp_p = []
            for i, v in json.loads(data).items():
                if symbol in v:
                    for k, l in v.items():
                        [temp_p.append((z, i)) for z in l if symbol in k]

            a = ['{}/{}'.format(symbol, i) for i in [j[0] for j in temp_p]]
            self.pairs.extend(list(set(a)))
