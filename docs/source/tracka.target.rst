########################################
Target manipulation and automatic TA.
########################################

***************************
Public Target List.
***************************

Add a New Coin.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PublicTarget.add

Compact List.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PublicTarget.t

Specific Coin information.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PublicTarget.show

Remove a coin.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PublicTarget.remove

Delete Everything.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PublicTarget.deleteAll

Set Alert Preference.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PublicTarget.setAlert

**************************
Private Target List.
**************************
Add a New Coin.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PrivateTarget.add

Compact List.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PrivateTarget.pv

Specific Coin information.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PrivateTarget.show

Remove a coin.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PrivateTarget.remove

Delete Everything.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PrivateTarget.deleteAll

Set Alert Preference.
__________________________________________
.. automethod:: tracka.bot_commands.targets.targets.PrivateTarget.setAlert

**************************
Public Server Target List.
**************************

For Everyone
_____________________________

Compact List.
=========================================
.. automethod:: tracka.bot_commands.targets.targets.PublicOneList.pub

Specific Coin information.
=========================================
.. automethod:: tracka.bot_commands.targets.targets.PublicOneList.show

Only For Admins
_____________________________

Add a New Coin.
=========================================
.. automethod:: tracka.bot_commands.targets.targets.PublicOneList.add

Remove a coin.
=========================================
.. automethod:: tracka.bot_commands.targets.targets.PublicOneList.remove

Delete Everything.
=========================================
.. automethod:: tracka.bot_commands.targets.targets.PublicOneList.deleteAll

