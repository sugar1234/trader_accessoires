##############################
Feedback
##############################

.. currentmodule:: tracka.bot_commands.feedback.feedback


***************************
For members.
***************************

Leave Feedback.
____________________________

.. automethod:: FeedbackCommand.add


***************************
For admins.
***************************


Get Anonymous Feedback.
___________________________
.. automethod:: FeedbackCommand.get_anon

Get All Feedback.
___________________________
.. automethod:: FeedbackCommand.get_all

Get Public Feedback.
___________________________
.. automethod:: FeedbackCommand.get_public

Set Alert.
__________________________
.. automethod:: FeedbackCommand.set_alert

Export.
__________________________
.. automethod:: FeedbackCommand.export

Delete all.
__________________________
.. automethod:: FeedbackCommand.delete_all
