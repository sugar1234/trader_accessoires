Commands for the concerned server Admin.
-----------------------------------------

Announce.
___________________________________________
.. automethod:: tracka.bot_commands.announcements.AnnounceCommand.send

Kick Group.
___________________________________________
.. automethod:: tracka.bot_commands.announcements.AnnounceCommand.kick_group

Kick Group Advanced.
___________________________________________
.. automethod:: tracka.bot_commands.announcements.AnnounceCommand.kick_group_adv

