"""Unit tests for target commands, and semi integration testing for discord bot."""
# coding=utf-8
from unittest import TestCase

import discord
from mongoengine import connect


class TestPublic(TestCase):
    """Test all the discord public commands."""

    def setUp(self):
        """Setup database and discord connection."""
        connect('test')
        self.bot = discord.Client()
        self.bot_id = 415428445657497600

    def test_a(self):
        def pred(m):
            return self.channel == m.channel

        @self.bot.event
        async def on_ready():
            """Fire as soon as the bot is online."""
            await self.bot.wait_until_ready()
            self.channel = self.bot.get_channel(422339617824702474)
            commands = ['add eth/btc 0.1 com bitfinex', 'add bmc 1 com1 bittrex', 'add eth/btc 0.2,0.3 com3,com4 binance', '', 'show eth', 'show eth/btc', 'show /btc', 'show bmc', 'remove eth',
                        'deleteAll']
            for command in commands:
                await self.channel.send(f'?t {command}')
                await self.bot.wait_for('message', check=pred)
            self.bot.loop.stop()

        try:
            self.bot.run('NDE1NDI4NDQ1NjU3NDk3NjAw.DYaGgQ.eAUicv6GTwpX_qu9BUHVP7upqpw')
        except RuntimeError:
            pass
