# coding=utf-8
from unittest import TestCase

import discord
from mongoengine import connect

import tracka.bot_commands.feedback.feedback_logic as logic
from tracka.item_classes.feedback import Feedback, FeedbackAlert

author_id = 329423423
author_id2 = 423423423
message_public = '?feedback add message'
message_anon = '?feedback add message -anonymous'
name = 'gaigai'
server_id = 12321321


def common_asserts(data, is_anonymous, is_seen):
    """Some common asserts for all tests."""
    assert data.first().author_id == author_id
    assert data.first().content == message_public.replace('?feedback add', '').replace('-anonymous', '').strip()
    assert data.first().is_anonymous is is_anonymous
    assert data.first().is_seen is is_seen


class TestFeedbackAlerts(TestCase):
    def setUp(self):
        """Setup database for test."""
        connect('test', host='mongo')
        # connect('test')
        self.fd = logic.FeedbackLogic()

    def test_set_alert_normal(self):
        start, increment = self.fd.set_alert_fun(2, 15, author_id, server_id)
        assert start == 2
        assert increment == 15
        user_prefs = FeedbackAlert.objects(author_id=author_id, server_id=server_id).first()
        assert user_prefs.start == 2
        assert user_prefs.increment == 15

    def test_set_alert_with_no_increment(self):
        start, increment = self.fd.set_alert_fun(2, None, author_id, server_id)
        assert start == 2
        assert increment == start
        user_prefs = FeedbackAlert.objects(author_id=author_id, server_id=server_id).first()
        assert user_prefs.start == 2
        assert user_prefs.increment == start

    def test_set_alert_with_0_increment(self):
        start, increment = self.fd.set_alert_fun(2, 0, author_id2, server_id)
        assert start == 2
        assert increment == 10000
        user_prefs = FeedbackAlert.objects(author_id=author_id2, server_id=server_id).first()
        assert user_prefs.start == 2
        assert user_prefs.increment == 10000


class TestFeedbackPublic(TestCase):
    """Test feedback backend for public messages."""

    def setUp(self):
        """Setup database for test."""
        connect('test', host='mongo')
        self.fd = logic.FeedbackLogic()

    def tearDown(self):
        """ Clear up after every test."""
        Feedback.objects().delete()

    def test_add_and_query_for_new(self):
        self.fd.log_all(message_public, author_id, server_id, name)
        data = self.fd.query_database(is_new=True, server_id=server_id)
        common_asserts(data, False, False)
        data = self.fd.query_database(server_id=server_id)
        self.fd.format_feedback(data, name)
        common_asserts(data, False, True)

    def test_add_empty_message(self):
        self.fd.log_all('', author_id, server_id, name)
        data = self.fd.query_database(is_new=True, server_id=server_id)
        assert 'Nothing found.' in self.fd.format_feedback(data, name)[0]

    def test_add_and_query_for_all(self):
        self.fd.log_all(message_public, author_id, server_id, name)
        data = self.fd.query_database(server_id=server_id)
        common_asserts(data, False, False)
        data = self.fd.query_database(server_id=server_id)
        self.fd.format_feedback(data, name)
        common_asserts(data, False, True)


class TestFeedbackAnon(TestCase):
    """ Test feedback backend for anonymous feedback."""

    def setUp(self):
        """Setup database for test."""
        connect('test', host='mongo')
        self.fd = logic.FeedbackLogic()

    def tearDown(self):
        """ Clear up after every test."""
        Feedback.objects().delete()

    def test_add_and_get_new_anonymous(self):
        self.fd.log_all(message_anon, author_id, server_id, name)
        data = self.fd.query_database(is_new=True, is_anonymous=True, server_id=server_id)
        common_asserts(data, True, False)
        data = self.fd.query_database(is_anonymous=True, server_id=server_id)
        self.fd.format_feedback(data, name)
        common_asserts(data, True, True)

    def test_add_and_get_all_anonymous(self):
        self.fd.log_all(message_anon, author_id, server_id, name)
        data = self.fd.query_database(is_new=True, is_anonymous=True, server_id=server_id)
        common_asserts(data, True, False)
        data = self.fd.query_database(is_anonymous=True, server_id=server_id)
        self.fd.format_feedback(data, name)
        common_asserts(data, True, True)


class TestFeedbackALL(TestCase):
    """ Test feedback backend for anonymous feedback."""

    def setUp(self):
        """Setup database for test."""
        connect('test', host='mongo')
        self.fd = logic.FeedbackLogic()

    def tearDown(self):
        """ Clear up after every test."""
        Feedback.objects().delete()

    def test_add_and_get_new_all(self):
        self.fd.log_all(message_anon, author_id, server_id, name)
        data = self.fd.query_database(is_new=True, is_all=True, server_id=server_id)
        common_asserts(data, True, False)
        data = self.fd.query_database(is_all=True, server_id=server_id)
        self.fd.format_feedback(data, name)
        common_asserts(data, True, True)

    def test_add_and_get_all(self):
        self.fd.log_all(message_anon, author_id, server_id, name)
        data = self.fd.query_database(is_new=True, is_all=True, server_id=server_id)
        common_asserts(data, True, False)
        data = self.fd.query_database(is_all=True, server_id=server_id)
        self.fd.format_feedback(data, name)
        common_asserts(data, True, True)


class TestFeedbackMisc(TestCase):
    """ Test feedback backend for export etc.."""

    def setUp(self):
        """Setup database for test."""
        connect('test', host='mongo')
        self.fd = logic.FeedbackLogic()

    def test_exporting_public(self):
        self.fd.log_all(message_public, author_id, server_id, name)
        self.fd.export_logic(server_id, 'filename')
        with open('filename', 'r') as f:
            csv_cont = f.read()
        assert 'message' in csv_cont

    def test_exporting_anonymous(self):
        self.fd.log_all(message_anon, author_id, server_id, name)
        self.fd.export_logic(server_id, 'filename')
        with open('filename', 'r') as f:
            csv_cont = f.read()
        assert 'message' in csv_cont


class TestFeedbackBot(TestCase):
    """Test discord integration"""

    def setUp(self):
        """Setup database and discord bot."""
        self.bot = discord.Client()
        self.bot_id = 415428445657497600
        self.alerta_id = 399564752801497089

    def test_a(self):
        def pred(m):
            """ Use check for wait for message."""
            return self.channel == m.channel

        @self.bot.event
        async def on_ready():
            """ Get's called as soon as the bot connects."""
            await self.bot.wait_until_ready()
            self.channel = self.bot.get_channel(422339617824702474)
            commands = ['add yo', 'add yo -anonymous', 'getAll -new', 'getAll', 'add yo', 'getpub -new', 'get-pub', 'add yo -anonymous', 'get_anon -new', 'getanon', 'delall', 'add yo',
                        'set_alert 20 0', 'alert 20',
                        'set-alert 20 20', 'alert 0', 'alert 0 20', 'add', 'add ', '', 'export']
            for command in commands:
                await self.channel.send(f'?feedback {command}')
                await self.bot.wait_for('message', check=pred)
            self.bot.loop.stop()

        try:
            self.bot.run('NDE1NDI4NDQ1NjU3NDk3NjAw.DYaGgQ.eAUicv6GTwpX_qu9BUHVP7upqpw')
        except Exception:
            pass
