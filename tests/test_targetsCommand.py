"""Unit tests for target commands, and semi integration testing for discord bot."""
import asyncio
# coding=utf-8
from unittest import TestCase

from mongoengine import connect

from tracka.bot_commands.targets import targets_logic
from tracka.functions import loadexchange
from tracka.item_classes import exchange as ex
from tracka.item_classes import target
from tracka.item_classes import user_preferences

bot_id = 231719975319109633
server_id = 331719975319109634


connect('test', host='mongo')
def common_asserts(data, pair, price, comment, exchange, is_public):
    assert data.target_price == price
    assert data.author_id == bot_id
    assert data.is_public is is_public
    assert data.type == comment
    assert data.exchange == exchange
    assert data.ticker == pair


class ATestLoadExchange(TestCase):
    def setUp(self):
        self.loop = asyncio.get_event_loop()
        selk = loadexchange.Loadexchange(self.loop)
#
        async def load():
            await selk.main()
#
        self.loop.run_until_complete(load())
#
    def test(self):
        tickers = ex.Exchange.objects(name='coinmarketcap')[0].tickers
        assert isinstance(tickers, str)
#
    def tearDown(self):
        self.loop.close()


class TestTargetPrivateCommand(TestCase):
    """Test all the target private command backend."""

    def setUp(self):
        """Setup database."""
        self.tr = targets_logic.TargetsCommand()
        self.is_public = False

    def test_add_and_remove_normal_private(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla', 'bitfinex', self.is_public)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/BTC', 0.1, 'bla', 'bitfinex', self.is_public)

        text = self.tr.remove('eth', bot_id, '', self.is_public)
        assert 'Deleted' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, base='ETH')
        assert data.count() == 0
        return 0

    def test_add_and_remove_usd_private(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/$', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

        text = self.tr.remove('eth/', bot_id, '', self.is_public)
        assert 'Deleted' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, base='ETH', quote='USD')
        assert data.count() == 0
        return 0

    def test_add_and_remove_usd_private_another(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth$', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

        text = self.tr.remove('eth/usd', bot_id, '', self.is_public)
        assert 'Deleted' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, base='ETH', quote='USD')
        assert data.count() == 0
        return 0

    def test_add_and_remove_all_private(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

        text = self.tr.remove('', bot_id, '', self.is_public)
        assert 'history' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        assert data.count() == 0
        return 0

    def tearDown(self):
        target.Target.objects().delete()


class TestTargetPublicPersonalCommand(TestCase):
    """Test all the target private command backend."""

    def setUp(self):
        """Setup database."""
        self.tr = targets_logic.TargetsCommand()
        self.is_public = True

    def test_add_and_remove_public(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla', 'bitfinex', self.is_public)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/BTC', 0.1, 'bla', 'bitfinex', self.is_public)

        text = self.tr.remove('eth/btc', bot_id, 'bla', self.is_public)
        assert 'Deleted' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, base='ETH')
        assert data.count() == 0
        return 0

    def test_add_and_remove_public_2(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/$', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

        text = self.tr.remove('eth', bot_id, 'bla', self.is_public)
        assert 'specify' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, base='ETH', quote='USD')
        assert data.count() == 1
        return 0

    def test_add_and_remove_public_3(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth$', '0.1', 'bla', 'bittrex', self.is_public)
        assert "Added" in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

        text = self.tr.remove('/usd', bot_id, '', self.is_public)
        assert 'Deleted' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, base='ETH', quote='USD')
        assert data.count() == 0
        return 0

    def tearDown(self):
        target.Target.objects().delete()


class TestMisc(TestCase):

    def setUp(self):
        """Setup database."""
        self.tr = targets_logic.TargetsCommand()
        self.is_public = True

    def test_add_wrong_exchange(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth$', '0.1', 'bla', 'inex', self.is_public)
        assert 'not currently supported' in text

    def test_not_existing_pair_in_given_exchange(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla', 'coinmarketcap', self.is_public)
        assert "Added" in text

    def test_add_to_server(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        assert 'Added' in text
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public, server_id=server_id).get()
        common_asserts(data, 'ETH/BTC', 0.1, 'bla', 'coinmarketcap', self.is_public)

    def test_add_multiple_same_ones(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        assert "Added" in text
        text = self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        assert "Please add a unique comment" in text

    def test_fix_coinmarketcap_pais(self):
        text = self.tr.add(bot_id, 'koloxarto', 'bqx/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public).get()
        assert data.base == 'ETHOS'
        assert 'BQX' in text

    def test_get_info(self):
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.12', 'bla2', 'coinmarketcap', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        text = self.tr.get_info(data, False)
        assert isinstance(text, list)

    def test_empty_get_info(self):
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        text = self.tr.get_info(None, False)
        assert 'Nothing found' in text
        assert isinstance(text, str)

    def test_get_info_with_other_exchanges(self):
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.1', 'bla', 'binance', self.is_public, server_id=server_id)
        self.tr.add(bot_id, 'koloxarto', 'eth/btc', '0.1', 'bla1', 'binance', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        text = self.tr.get_info(data, False)
        assert isinstance(text, list)

    def test_get_info_for_usd_as_quote(self):
        self.tr.add(bot_id, 'koloxarto', 'ltc/usd', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        self.tr.get_info(data, False)

    def test_get_info_compact(self):
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.12', 'bla2', 'coinmarketcap', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        text = self.tr.get_info(data, True)
        assert isinstance(text, str)

    def test_get_info_compact_with_lots(self):
        self.tr.add(bot_id, 'koloxarto', 'ltc/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        self.tr.add(bot_id, 'koloxarto', 'bmc/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        self.tr.add(bot_id, 'koloxarto', 'xrp/btc', '0.1', 'bla', 'coinmarketcap', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        text = self.tr.get_info(data, True)
        assert isinstance(text, str)

    def test_add_wrong_coin(self):
        text = self.tr.add(bot_id, 'koloxarto', 'ttt/btc', '0.1', 'bla', 'binance', self.is_public, server_id=server_id)
        assert 'does not offer this pair' in text

    def test_add_exchange_without_quote_volume(self):
        self.tr.add(bot_id, 'koloxarto', 'btc/usdt', '0.1', 'bla', 'bitfinex', self.is_public, server_id=server_id)
        data = target.Target.objects(author_id=bot_id, is_public=self.is_public)
        text = self.tr.get_info(data, False)
        assert isinstance(text, list)


    def tearDown(self):
        target.Target.objects().delete()


class TestShower(TestCase):

    def setUp(self):
        """Setup database."""
        self.tr = targets_logic.TargetsCommand()
        self.is_public = True

    def test_shower_a(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = self.tr.shower('eth', bot_id, self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

    def test_shower_b(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = self.tr.shower('eth/', bot_id, self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

    def test_shower_c(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = self.tr.shower('/usd', bot_id, self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

    def test_shower_d(self):
        text = self.tr.add(bot_id, 'koloxarto', 'eth', '0.1', 'bla', 'bittrex', self.is_public)
        assert 'Added' in text
        data = self.tr.shower('eth/usd', bot_id, self.is_public).get()
        common_asserts(data, 'ETH/USD', 0.1, 'bla', 'bittrex', self.is_public)

    def tearDown(self):
        target.Target.objects().delete()


class TestSetAlert(TestCase):

    def setUp(self):
        """Setup database and discord connection."""
        self.tr = targets_logic.TargetsCommand()
        self.up = user_preferences.UserPref

    def test_add_alert_public(self):
        text = self.tr.set_alert(bot_id, 'koloxarto', 'on', True)
        assert 'public targets' in text
        data = self.up.objects().get()
        assert data.t_alerts is True

    def test_add_alert_private(self):
        text = self.tr.set_alert(bot_id, 'koloxarto', 'on', False)
        assert 'private targets' in text
        data = self.up.objects().get()
        assert data.pv_alerts is True

    def test_remove_alert_public(self):
        text = self.tr.set_alert(bot_id, 'koloxarto', 'off', True)
        assert 'public targets' in text
        data = self.up.objects().get()
        assert data.t_alerts is False

    def test_remove_alert_public(self):
        text = self.tr.set_alert(bot_id, 'koloxarto', 'off', False)
        assert 'private targets' in text
        data = self.up.objects().get()
        assert data.pv_alerts is False

    def test_false_values(self):
        text = self.tr.set_alert(bot_id, 'koloxarto', '124', False)
        assert 'Please use' in text
        data = self.up.objects()
        assert data.count() == 0

    def tearDown(self):
        self.up.objects().delete()
